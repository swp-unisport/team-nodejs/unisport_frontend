import React from 'react'
import { act, render, wait } from '@testing-library/react'
import { Provider } from 'react-redux'
import { store } from './redux/store'
import App from './App'
import { MemoryRouter, Route } from 'react-router'
import { Location } from 'history'

// test('renders user page', () => {
//     const renderedApp = render(
//         <Provider store={store}>
//             <MemoryRouter>
//                 <App />
//             </MemoryRouter>
//         </Provider>
//     )
//     expect(
//         renderedApp.getByText(
//             'Dies ist ein Assistent zur Auswahl der für dich passenden Sportart.'
//         )
//     ).toBeInTheDocument()
// })

jest.setTimeout(13000)
test('admin page navigation', async () => {
    let testLocation: Location

    const renderedApp = render(
        <Provider store={store}>
            <MemoryRouter initialEntries={['/admin/sportarten']}>
                <App />
                <Route
                    path="*"
                    render={({ history, location }) => {
                        testLocation = location
                        return null
                    }}
                />
            </MemoryRouter>
        </Provider>
    )

    await wait()

    let drawer = renderedApp!.container.querySelector('#login-view')
    expect(drawer).not.toBeNull()
})
