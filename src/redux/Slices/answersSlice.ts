import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IAnswer, IQuestion } from '../../types/ApiTypes'

interface answersState {
    value: IAnswer[]
}
//may have to change array length, it is now statically set to 10
const defaultAnswers: IAnswer[] = Array.from(Array(10).keys()).map((index) => {
    return {
        answer: -1,
        category: '',
        index: index,
        weight: 0
    }
})
const initialState: answersState = {
    value: defaultAnswers
}
export const answersSlice = createSlice({
    name: 'answersData',
    initialState,
    reducers: {
        updateAnswers: (state, action: PayloadAction<IAnswer>) => {
            const newState = [...state.value]
            newState[action.payload.index] = action.payload
            return { ...state, value: newState }
        }
    }
})

export const { updateAnswers } = answersSlice.actions
export default answersSlice.reducer
