import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IActivity, Language } from '../../types/ApiTypes'

interface languageState {
    value: Language
}

let defaultLang = Language.GERMAN
// try to detect language
if (navigator.language.startsWith('en')) {
    defaultLang = Language.ENGLISH
} else if (navigator.language.startsWith('fr')) {
    defaultLang = Language.FRENCH
}

console.log(`Init with detected language "${defaultLang}"`)

const initialState: languageState = {
    value: defaultLang
}

export const languageSlice = createSlice({
    name: 'userLanguage',
    initialState,
    reducers: {
        updateLanguage: (state, action: PayloadAction<Language>) => {
            state.value = action.payload
        }
    }
})

export const { updateLanguage } = languageSlice.actions
export default languageSlice.reducer
