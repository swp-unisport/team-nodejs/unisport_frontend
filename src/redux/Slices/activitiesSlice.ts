import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IActivity } from '../../types/ApiTypes'

interface activitiesState {
    value: IActivity[]
}

const initialState: activitiesState = {
    value: []
}

export const activitiesSlice = createSlice({
    name: 'activitiesData',
    initialState,
    reducers: {
        updateActivities: (state, action: PayloadAction<IActivity[]>) => {
            state.value = action.payload
        }
    }
})

export const { updateActivities } = activitiesSlice.actions
export default activitiesSlice.reducer
