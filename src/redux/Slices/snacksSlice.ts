import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ISnack } from '../../types/ApiTypes'

interface snacksState {
    value: ISnack[]
}

const initialState: snacksState = {
    value: []
}

export const snacksSlice = createSlice({
    name: 'snacksData',
    initialState,
    reducers: {
        updateSnacks: (state, action: PayloadAction<ISnack[]>) => {
            state.value = action.payload
        }
    }
})

export const { updateSnacks } = snacksSlice.actions
export default snacksSlice.reducer
