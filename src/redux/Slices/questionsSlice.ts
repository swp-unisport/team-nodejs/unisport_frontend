import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IQuestion } from '../../types/ApiTypes'

interface questionsState {
    value: IQuestion[]
}

const initialState: questionsState = {
    value: []
}

export const questionsSlice = createSlice({
    name: 'questionsData',
    initialState,
    reducers: {
        updateQuestions: (state, action: PayloadAction<IQuestion[]>) => {
            state.value = action.payload
        }
    }
})

export const { updateQuestions } = questionsSlice.actions
export default questionsSlice.reducer
