import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ICategorie } from '../../components/admin/shared/ICategorie'
import { ISports } from '../../types/ApiTypes'

interface categoriesState {
    value: ICategorie[]
}

const initialState: categoriesState = {
    value: []
}

export const categoriesSlice = createSlice({
    name: 'categoriesData',
    initialState,
    reducers: {
        updateCategories: (state, action: PayloadAction<ICategorie[]>) => {
            state.value = action.payload
        },
        updateCategoriesFromSport: (state, action: PayloadAction<ISports>) => {
            state.value = Object.keys(action.payload.categoryWeights)
        }
    }
})

export const {
    updateCategories,
    updateCategoriesFromSport
} = categoriesSlice.actions
export default categoriesSlice.reducer
