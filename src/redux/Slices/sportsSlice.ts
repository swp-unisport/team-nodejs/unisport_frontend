import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ISports } from '../../types/ApiTypes'

interface sportsState {
    value: ISports[]
}

const initialState: sportsState = {
    value: []
}

export const sportsSlice = createSlice({
    name: 'sportsData',
    initialState,
    reducers: {
        updateSports: (state, action: PayloadAction<ISports[]>) => {
            state.value = action.payload
        }
    }
})

export const { updateSports } = sportsSlice.actions
export default sportsSlice.reducer
