import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { OrderedPages } from '../../types/ApiTypes'

interface orderedPagesState {
    value: string[]
}

const initialState: orderedPagesState = {
    value: []
}

export const orderedPagesSlice = createSlice({
    name: 'orderedPagesData',
    initialState,
    reducers: {
        updateOrderedPages: (state, action: PayloadAction<string[]>) => {
            state.value = action.payload
        }
    }
})

export const { updateOrderedPages } = orderedPagesSlice.actions
export default orderedPagesSlice.reducer
