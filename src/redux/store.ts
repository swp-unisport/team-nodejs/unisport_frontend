import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit'
import sportsReducer from './Slices/sportsSlice'
import questionsReducer from './Slices/questionsSlice'
import snacksReducer from './Slices/snacksSlice'
import orderedPagesReducer from './Slices/orderedPagesSlice'
import activitiesReducer from './Slices/activitiesSlice'
import answersReducer from './Slices/answersSlice'
import categoriesReducer from './Slices/categoriesSlice'
import languageReducer from './Slices/languageSlice'

export const store = configureStore({
    reducer: {
        categoriesData: categoriesReducer,
        sportsData: sportsReducer,
        activitiesData: activitiesReducer,
        snacksData: snacksReducer,
        orderedPagesData: orderedPagesReducer,
        questionsData: questionsReducer,
        answersData: answersReducer,
        languageData: languageReducer
    }
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
    ReturnType,
    RootState,
    unknown,
    Action<string>
>
