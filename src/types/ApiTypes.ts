type ObjectId = string

export enum Language {
    ENGLISH = 'en',
    GERMAN = 'de',
    FRENCH = 'fr'
}

export type TranslatedString = {
    [lang in Language]: string
}

/**
 * for view 'Übersetzungen' and app translation
 */
export interface AppTranslations {
    key1: TranslatedString
}

export interface ISports {
    _id: string
    name: string
    categoryWeights: { [categoryId: string]: number | null }
    url: string
    active: boolean
}

/**
 * returned by api call to get all sports
 */
export type SportsList = ISports[]

// == STATISTICS

export type ClicksPerSport = {
    [sport: string]: number
}

/**
 * use pagination in backend
 */
export type SportsRanking = {
    name: string
    relativ: number
    total: number
}[]

export type DiscontinuationRate = {
    name: string
    percentage: number
    type: 'question' | 'snack' | 'activity'
}[]

/**
 * returned by backend
 */
export type AllQuestions = IQuestion[]

/**
 * TODO: store ObjectIds or just name-strings?
 */
export interface SyncData {
    newSports: ObjectId[]
    returningSports: ObjectId[]
    archivedSports: ObjectId[]
}

// === User frontend interfaces === //

/**
 * `imageUrl`: should point to local endpoint in best case, to be independent from other image servers
 */

export interface LandingPage {
    type: 'landing'
    text: string
    imageUrl: string
}

export interface SnackUserPage {
    type: 'snack'
    imageUrl: string
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    active: boolean
}

// == QUESTIONS
export interface IQuestion {
    _id: ObjectId
    descriptions: TranslatedString
    active: boolean
    category: string
}

export type QuestionUserPage = { type: 'question' } & IQuestion

export interface ISnack {
    title: string
    _id: ObjectId
    imageUrl: string
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    active: boolean
    totalViews: number
    totalDurationOnPage: number
}

export interface IActivity {
    _id: ObjectId
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    duration: number
    totalViews: number
    totalDurationOnPage: number
    imageUrl: string
    title: string
}

interface ActivityUserPage {
    type: 'activity'
    descriptions: TranslatedString // {de: "bsp", en: "ex.", fr:"..."}
    duration: number
    imageUrl: string
}

export type Page = (
    | LandingPage
    | SnackUserPage
    | QuestionUserPage
    | ActivityUserPage
) & { id: string }

/**
 * order of views / pages
 */
export interface ViewTrackingPostBody {
    viewId: string
    timeOnPage: number
}

export type OrderedPages = {
    title: string
    type: 'snack' | 'activity' | 'question'
    id: ObjectId
}[]

/**
 * returned by get /api/image
 */
export type ImagesInformationResponse = {
    url: string
    uploaded: string // iso date-time
    activeUsages: number // how often this is selected
}[]

export interface IAnswer {
    index: number
    category: string
    answer: number
    weight: number
}

export interface ISportResult {
    name: string
    score: number
    url: string
}
