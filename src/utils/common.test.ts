import { typedEntries } from './common'

describe('utils/common', () => {
    it('typedEntries should return entries', () => {
        const testData = { key1: 1, key2: 2 }
        expect(typedEntries(testData)).toEqual([
            ['key1', 1],
            ['key2', 2]
        ] as [keyof typeof testData, number][])
    })
})
