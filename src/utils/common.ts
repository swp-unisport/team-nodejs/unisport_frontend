export function typedEntries<T>(obj: T): [keyof T] {
    return Object.entries(obj) as any
}

export async function adminFetch(
    url: RequestInfo,
    params: RequestInit = {}
): Promise<Response> {
    const token = localStorage.getItem('token')
    if (!token) return Promise.reject('not logged in')
    return fetch(url, {
        ...params,
        headers: {
            ...(params.headers || {}),
            Authentication: `Bearer ${token}`
        }
    })
}
