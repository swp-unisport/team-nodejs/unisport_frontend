import { CardMedia, Grid, makeStyles } from '@material-ui/core'
import logo from '../../assets/unisport_wide_logo.png'
import React from 'react'

const useStyles = makeStyles((theme) => ({
    logo: {
        marginTop: '1%',
        marginBottom: '6%',
        paddingLeft: '20px',
        paddingRight: '20px'
    }
}))

const Logo = () => {
    const classes = useStyles()
    return (
        <Grid item xs={12} md={10}>
            <CardMedia
                className={classes.logo}
                component="img"
                image={logo}
                title="UniSport Logo"
                alt="UniSport_Logo"
            />
        </Grid>
    )
}
export default Logo
