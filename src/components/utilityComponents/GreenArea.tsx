import { Box, makeStyles, Paper } from '@material-ui/core'
import React, { CSSProperties } from 'react'

const useStyles = makeStyles((theme) => ({
    paper: {
        padding: '20px',
        textAlign: 'center',
        backgroundColor: '#8bc34a',
        marginLeft: '10%',
        marginRight: '10%',
        marginBottom: '60px'
    }
}))

interface IGreenArea {
    children: JSX.Element | JSX.Element[]
    style?: CSSProperties
}

const GreenArea = ({ children, style }: IGreenArea) => {
    const classes = useStyles()
    return (
        <Box width={1}>
            <Paper style={style} className={classes.paper}>
                {children}
            </Paper>
        </Box>
    )
}
export default GreenArea
