import React from 'react'
import { useWizard } from 'react-use-wizard'

let startTimeOnPage = Date.now()

interface TrackableViewProps {
    viewId: string
}

/**
 * Use in Wizard
 * sends info to server on pageleave
 */
const TrackableView: React.FC<TrackableViewProps> = ({ children, viewId }) => {
    const { handleStep } = useWizard()

    handleStep(() => {
        const timeOnPage = (Date.now() - startTimeOnPage) / 1000

        fetch('/api/nextStep', {
            method: 'post',
            body: JSON.stringify({
                viewId,
                timeOnPage
            })
        })

        console.log(`User spent ${timeOnPage}s on page ${viewId}`)

        startTimeOnPage = Date.now()
    })

    return <React.Fragment>{children}</React.Fragment>
}
