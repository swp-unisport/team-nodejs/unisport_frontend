import { Grow, Snackbar } from '@material-ui/core'
import React, { createRef } from 'react'
import { Alert } from '@material-ui/lab'
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline'
import { TransitionProps } from '@material-ui/core/transitions'

interface IAnswerAlert {
    toggle: boolean
}

const AnswerAlert: React.FC<IAnswerAlert> = ({ toggle }) => {
    const snackbarRef = createRef()

    function GrowTransition(props: TransitionProps) {
        return <Grow {...props} />
    }

    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
            ref={snackbarRef}
            open={toggle}
            TransitionComponent={GrowTransition}
        >
            <Alert
                iconMapping={{
                    error: <ErrorOutlineIcon fontSize="inherit" />
                }}
                severity="error"
            >
                Bitte beantworten Sie die Frage
            </Alert>
        </Snackbar>
    )
}
export default AnswerAlert
