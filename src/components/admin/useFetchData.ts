import { useAppDispatch } from '../../hooks'
import { updateSports } from '../../redux/Slices/sportsSlice'
import { updateOrderedPages } from '../../redux/Slices/orderedPagesSlice'
import { updateQuestions } from '../../redux/Slices/questionsSlice'
import { updateSnacks } from '../../redux/Slices/snacksSlice'
import { updateActivities } from '../../redux/Slices/activitiesSlice'
import { updateCategoriesFromSport } from '../../redux/Slices/categoriesSlice'
import { useEffect, useState } from 'react'
import { useLocation } from 'react-router-dom'
import { adminFetch } from '../../utils/common'

export const useFetchData = async () => {
    const location = useLocation()
    const [startedLoading, setStartedLoading] = useState(false)
    const dispatch = useAppDispatch()

    useEffect(() => {
        async function asyncInner() {
            const token = localStorage.getItem('token')
            if (
                !token ||
                startedLoading ||
                location.pathname.includes('/login')
            ) {
                return
            }
            setStartedLoading(true)

            console.log('fetchin all admin data')
            try {
                const [
                    sports,
                    orderedPages,
                    questions,
                    snacks,
                    activities
                ] = await Promise.all(
                    [
                        adminFetch('/api/sport', { method: 'GET' }),
                        adminFetch('/api/ordered', { method: 'GET' }),
                        adminFetch('/api/question', { method: 'GET' }),
                        adminFetch('/api/snacks', { method: 'GET' }),
                        adminFetch('/api/activity', { method: 'GET' })
                    ].map((res) => res.then((r) => r.json()))
                )

                console.log('got admin data, updating redux state')
                console.log(orderedPages[0].order)
                dispatch(updateSports(sports))
                // TODO weird result from backend
                dispatch(updateOrderedPages(orderedPages[0].order))
                dispatch(updateQuestions(questions))
                dispatch(updateSnacks(snacks))
                dispatch(updateActivities(activities))
                dispatch(updateCategoriesFromSport(sports[0]))

                console.log(sports, orderedPages, questions, snacks, activities)
            } catch (error) {
                console.error('failed to get admin data: ', error)
                alert(error)
            }
        }

        asyncInner()
    }, [location])
}
