import React, { useEffect, useRef, useState } from 'react'
import { Question } from './Question'
import { ListItem, List } from '@material-ui/core'
import AddIcon from '@material-ui/icons/Add'
import Divider from '@material-ui/core/Divider'
import { IQuestion } from '../../../types/ApiTypes'
import { action, CustomSpeedDial } from '../shared/CustomSpeedDial'
import SaveIcon from '@material-ui/icons/Save'
import { ICategorie } from '../shared/ICategorie'
import ObjectID from 'bson-objectid'
import { useAppDispatch, useAppSelector } from '../../../hooks'
import { ConfirmDialog } from '../shared/Dialogs/ConfirmDialog'
import { updateQuestions } from '../../../redux/Slices/questionsSlice'

export const QuestionView: React.FC = () => {
    const dialogConfirmState = useState(false)
    const questionsData = useAppSelector((state) => state.questionsData.value)
    const categoriesData = useAppSelector((state) => state.categoriesData.value)
    const [questions, setQuestions] = useState<IQuestion[]>([])
    const [categories, setCategories] = useState<ICategorie[]>([])
    const isLoading = questions === []
    const questionsRef: React.MutableRefObject<IQuestion[]> = useRef([])
    const dispatch = useAppDispatch()

    useEffect(() => {
        const copy = JSON.parse(JSON.stringify(questionsData))
        setQuestions(copy)
        questionsRef.current = copy
    }, [questionsData])
    useEffect(() => {
        setCategories(JSON.parse(JSON.stringify(categoriesData)))
    }, [categoriesData])

    const actions: action[] = [
        {
            icon: <SaveIcon />,
            title: 'speichern',
            handler: () => dialogConfirmState[1](true)
        },
        {
            icon: <AddIcon />,
            title: 'hinzufügen',
            handler: () => handleCategorieChange('add')
        }
    ]

    async function saveToDB() {
        await fetch('/api/question', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(questionsRef.current)
        }).then((e) => {
            if (e.ok) {
                dispatch(updateQuestions(questionsRef.current))
            }
        })
    }

    function handleCategorieChange(
        mode: 'add' | 'updateActive' | 'delete' | 'update',
        id?: string,
        question?: IQuestion
    ) {
        let copy = [...questions]
        if (mode === 'add') {
            copy.push({
                category: '',
                active: true,
                _id: new ObjectID().toHexString(),
                descriptions: { de: '', en: '', fr: '' }
            })
        }
        if (id !== undefined) {
            const index = questions.findIndex((e) => e._id === id)
            if (index === -1) return
            if (mode === 'delete') {
                copy.splice(index, 1)
            }
            if (question !== undefined && mode !== 'delete')
                copy[index] = question
            if (mode === 'updateActive') {
                copy[index].active = !copy[index].active
            }
        }
        questionsRef.current = copy
        setQuestions(copy)
    }

    return (
        <div>
            <h2>Aktive Fragen:</h2>
            {isLoading && <p>loading...</p>}
            {!isLoading && (
                <List>
                    {questions.map((data, idx) => {
                        if (data.active)
                            return (
                                <div key={data._id}>
                                    {' '}
                                    <ListItem>
                                        {' '}
                                        <Question
                                            handler={handleCategorieChange}
                                            questionData={data}
                                            questionRef={questionsRef}
                                            categories={categories}
                                            index={idx}
                                        />{' '}
                                    </ListItem>{' '}
                                    <Divider />{' '}
                                </div>
                            )
                        else return null
                    })}
                </List>
            )}
            <h2>Inaktive Fragen:</h2>
            {isLoading && <p>loading...</p>}
            {!isLoading && (
                <List>
                    {questions.map((data, idx) => {
                        if (!data.active)
                            return (
                                <div key={data._id}>
                                    {' '}
                                    <ListItem>
                                        {' '}
                                        <Question
                                            handler={handleCategorieChange}
                                            questionData={data}
                                            questionRef={questionsRef}
                                            categories={categories}
                                            index={idx}
                                        />{' '}
                                    </ListItem>{' '}
                                    <Divider />{' '}
                                </div>
                            )
                        else return null
                    })}
                </List>
            )}
            <ConfirmDialog
                handleCategorie={() => {
                    saveToDB()
                }}
                state={dialogConfirmState}
                text={{
                    header: 'Speichern',
                    body: 'Sollen die Änderungen gespeichert werden',
                    confirm: 'Bestätigen',
                    abort: 'Abbrechen'
                }}
            />
            <CustomSpeedDial actions={actions} />
        </div>
    )
}
