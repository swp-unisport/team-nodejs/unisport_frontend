import React, { ChangeEvent, useState } from 'react'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import Collapse from '@material-ui/core/Collapse'
import {
    Button,
    Select,
    Table,
    TableBody,
    TableCell,
    TableRow,
    TextField
} from '@material-ui/core'
import ExpandLess from '@material-ui/icons/ExpandLess'
import ExpandMore from '@material-ui/icons/ExpandMore'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import ClearIcon from '@material-ui/icons/Clear'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import { IQuestion, Language } from '../../../types/ApiTypes'
import { ICategorie } from '../shared/ICategorie'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        div: {
            width: '100%'
        },
        field: {
            marginLeft: '10px',
            maxWidth: '700px'
        },
        container: {
            display: 'inline-flex',
            width: '100%'
        },
        selectEmpty: {
            marginTop: theme.spacing(2)
        },
        select: {
            marginLeft: '10px',
            minWidth: '150px'
        },
        button: {
            marginLeft: '10px'
        }
    })
)

type QuestionProp = {
    index: number
    questionRef: React.MutableRefObject<IQuestion[]>
    questionData: IQuestion
    categories: ICategorie[]
    handler: (
        mode: 'add' | 'updateActive' | 'delete',
        id?: string,
        question?: IQuestion
    ) => void
}

const languages = [Language.ENGLISH, Language.FRENCH]

export const Question: React.FC<QuestionProp> = ({
    index,
    questionRef,
    questionData,
    categories,
    handler
}: QuestionProp) => {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    const [question, setQuestion] = useState<IQuestion>(questionData)

    const handleValueChange = (
        e: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
        lan: Language
    ) => {
        setQuestion({
            ...question,
            descriptions: { ...question.descriptions, [lan]: e.target.value }
        })
        questionRef.current[index].descriptions = {
            ...question.descriptions,
            [lan]: e.target.value
        }
    }

    return (
        <div className={classes.div}>
            <div className={classes.container}>
                <Button
                    variant="outlined"
                    onClick={() => setOpen(!open)}
                    className={classes.button}
                >
                    {open ? <ExpandLess /> : <ExpandMore />}
                </Button>
                <TextField
                    id="outlined-basic"
                    variant="outlined"
                    fullWidth
                    value={question.descriptions['de']}
                    className={classes.field}
                    onChange={(e) => handleValueChange(e, Language.GERMAN)}
                />
                <Select
                    variant="outlined"
                    className={classes.select}
                    value={question.category}
                    onChange={(e) => {
                        setQuestion({
                            ...question,
                            category: e.target.value as string
                        })
                        questionRef.current[index].category = e.target
                            .value as string
                    }}
                >
                    {categories.map((categorie) => {
                        return (
                            <option value={categorie} key={categorie}>
                                {categorie}
                            </option>
                        )
                    })}
                </Select>
                <Button
                    variant="outlined"
                    className={classes.button}
                    onClick={() => {
                        handler('updateActive', question._id, question)
                    }}
                >
                    {question.active && <ArrowDownwardIcon />}
                    {!question.active && <ArrowUpwardIcon />}
                </Button>
                <Button
                    variant="outlined"
                    className={classes.button}
                    onClick={() => {
                        handler('delete', question._id, question)
                    }}
                >
                    <ClearIcon />
                </Button>
            </div>
            {
                <Collapse in={open} timeout="auto" unmountOnExit>
                    <Table style={{ maxWidth: '700px' }}>
                        <TableBody>
                            {languages.map((entry: Language) => {
                                return (
                                    <TableRow key={entry}>
                                        <TableCell>
                                            <TextField
                                                fullWidth
                                                id="outlined-basic"
                                                variant="outlined"
                                                label={entry}
                                                value={
                                                    question.descriptions[entry]
                                                }
                                                onChange={(e) =>
                                                    handleValueChange(e, entry)
                                                }
                                            />
                                        </TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Collapse>
            }
        </div>
    )
}
