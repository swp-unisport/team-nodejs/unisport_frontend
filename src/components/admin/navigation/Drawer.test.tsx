import { render, wait } from '@testing-library/react'
import { createMemoryHistory } from 'history'
import React from 'react'
import { Router } from 'react-router'
import { OurDrawer } from './Drawer'

describe('admin/Navbar', () => {
    it('should route to correct paths', async () => {
        const history = createMemoryHistory({
            initialEntries: ['/admin/sportarten']
        })
        const renderedNavbar = render(
            <Router history={history}>
                <OurDrawer />
            </Router>
        )

        await wait()

        let drawer = renderedNavbar.container.querySelector('#admin-drawer')
        expect(drawer).not.toBeNull()
    })
})
