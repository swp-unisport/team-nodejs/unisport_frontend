import {
    useRouteMatch,
    LinkProps as RouterLinkProps,
    useLocation,
    NavLink
} from 'react-router-dom'
import React from 'react'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import Filter1Icon from '@material-ui/icons/Filter1'
import Filter2Icon from '@material-ui/icons/Filter2'
import Filter3Icon from '@material-ui/icons/Filter3'
import Filter4Icon from '@material-ui/icons/Filter4'
import Filter5Icon from '@material-ui/icons/Filter5'
import Filter6Icon from '@material-ui/icons/Filter6'
import Filter7Icon from '@material-ui/icons/Filter7'
import Divider from '@material-ui/core/Divider'
import { ListItemIcon, ListItemText } from '@material-ui/core'

// from https://material-ui.com/guides/composition/#link
interface ListItemLinkProps {
    icon: React.ReactElement
    primary: string
    to: string
}

const ListItemLink: React.FC<ListItemLinkProps> = (
    props: ListItemLinkProps
) => {
    const { icon, primary, to } = props

    const renderLink = React.useMemo(
        () =>
            // inner function must have a name
            React.forwardRef<any, Omit<RouterLinkProps, 'to'>>(
                function RenderedLink(itemProps, ref) {
                    return <NavLink to={to} ref={ref} {...itemProps} />
                }
            ),
        [to]
    )

    renderLink.displayName = 'RenderLink'

    let location = useLocation()

    return (
        <li>
            <ListItem
                button
                component={renderLink}
                selected={location.pathname === to}
            >
                <ListItemIcon>{icon}</ListItemIcon>
                <ListItemText primary={primary} />
            </ListItem>
        </li>
    )
}

export const drawerWidth = '220px'

export const OurDrawer: React.FC = () => {
    let { url } = useRouteMatch()

    return (
        <Drawer
            id="admin-drawer"
            variant="permanent"
            style={{ width: drawerWidth }}
        >
            <List style={{ width: drawerWidth, marginTop: '60px' }}>
                <ListItemLink
                    to={`${url}/sportarten`}
                    primary="Sportarten"
                    icon={<Filter1Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/statistiken`}
                    primary="Statistik"
                    icon={<Filter2Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/fragen`}
                    primary="Fragen"
                    icon={<Filter3Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/synchronisieren`}
                    primary="Synchronisieren"
                    icon={<Filter4Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/snacks`}
                    primary="Snacks & Co"
                    icon={<Filter5Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/reinfolge`}
                    primary="Reihenfolge"
                    icon={<Filter6Icon />}
                />
                <Divider />
                <ListItemLink
                    to={`${url}/translation`}
                    primary="Übersetzungen"
                    icon={<Filter7Icon />}
                />
                <Divider />
            </List>
        </Drawer>
    )
}
