import React, { useEffect, useState } from 'react'
import { useAppSelector } from '../../../hooks'
import { IActivity, ISnack } from '../../../types/ApiTypes'

export const useFetchSnacksAndActivities = (): [
    content: any,
    setContent: any
] => {
    const snacks = useAppSelector((state) => state.snacksData.value) as ISnack[]
    const activities = useAppSelector(
        (state) => state.activitiesData.value
    ) as IActivity[]
    const [changesSnacks, setChangesSnacks] = useState([])
    const [changedActivities, setChangesActivities] = useState([])
    useEffect(() => {
        let c: any = []
        snacks.forEach((e) => {
            c.push({ type: 'snack', ...e })
        })
        setChangesSnacks(c)
    }, [snacks])
    useEffect(() => {
        let c: any = []
        activities.forEach((e) => {
            c.push({ type: 'activity', ...e })
        })
        console.log('activities changed', c)
        setChangesActivities(c)
    }, [activities])

    const [content, setContent] = useState<(ISnack | IActivity)[]>([])
    useEffect(() => {
        setContent(changesSnacks.concat(changedActivities))
        //setContent(snacks)
    }, [changesSnacks, changedActivities])

    return [content, setContent]
}
