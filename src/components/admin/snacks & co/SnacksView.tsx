import React, { useState } from 'react'
import AddIcon from '@material-ui/icons/Add'
import { SnackTable } from './SnackTable'
import { useFetchSnacksAndActivities } from './useFetchSnacksAndActivities'
import { ConfirmDialog } from '../shared/Dialogs/ConfirmDialog'
import { action, CustomSpeedDial } from '../shared/CustomSpeedDial'
import SaveIcon from '@material-ui/icons/Save'
import { AddDialog } from './Dialogs/AddDialog'
import ObjectID from 'bson-objectid'
import { IActivity, ISnack } from '../../../types/ApiTypes'
import { updateSnacks } from '../../../redux/Slices/snacksSlice'
import { useAppDispatch } from '../../../hooks'
import { updateActivities } from '../../../redux/Slices/activitiesSlice'

export const SnacksView: React.FC = () => {
    const [pages, setPages] = useFetchSnacksAndActivities()
    const dialogConfirmState = useState(false)
    const dialogAddState = useState(false)
    const dispatch = useAppDispatch()
    const actions: action[] = [
        {
            handler: () => dialogConfirmState[1](true),
            icon: <SaveIcon />,
            title: 'Speichern'
        },
        {
            handler: () => dialogAddState[1](true),
            icon: <AddIcon />,
            title: 'Hinzufügen'
        }
    ]

    async function saveToDB() {
        let snacks: ISnack[] = []
        let activities: IActivity[] = []
        pages.forEach((p: any) => {
            const { type, ...page } = p
            if (type === 'snack') {
                snacks.push(page)
            }
            if (type === 'activity') {
                activities.push(page)
            }
        })
        const resOne = await fetch('/api/snacks', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(snacks)
        }).then((e) => {
            console.log(e)
            if (e.ok) {
                dispatch(updateSnacks(snacks))
            }
        })
        const resTwo = await fetch('/api/activities', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(activities)
        }).then((e) => {
            console.log(e)
            if (e.ok) {
                dispatch(updateActivities(activities))
            }
        })
    }

    function addSnack(type: 'snack' | 'activity', title: string) {
        let copy = [...pages]
        copy.push({
            title,
            _id: new ObjectID().toHexString(),
            active: true,
            type,
            imageUrl: '',
            descriptions: { de: '', en: '', fr: '' },
            totalDurationOnPage: 0,
            totalViews: 0
        })
        setPages(copy)
    }

    return (
        <div>
            <h2>Aktive Snacks/Aktivitäten</h2>

            <SnackTable setData={setPages} data={pages} active={true} />

            <h2>Inaktive Snacks/Aktivitäten</h2>
            <SnackTable setData={setPages} data={pages} active={false} />

            <ConfirmDialog
                handleCategorie={saveToDB}
                state={dialogConfirmState}
                text={{
                    header: 'Speichern',
                    body: 'Sollen die Änderungen gespeichert werden',
                    confirm: 'Bestätigen',
                    abort: 'Abbrechen'
                }}
            />
            <AddDialog
                handleCategorie={addSnack}
                state={dialogAddState}
                text={{
                    label: 'Titel',
                    header: 'Hinzufügen',
                    body: 'Bitte gib Typ und Titel ein',
                    confirm: 'Hinzufügen',
                    abort: 'Abbrechen'
                }}
            />
            <CustomSpeedDial actions={actions} />
        </div>
    )
}
