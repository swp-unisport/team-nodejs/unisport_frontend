import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { MenuItem, Select } from '@material-ui/core'

interface FormDialogProps {
    handleCategorie: (type: 'snack' | 'activity', title: string) => void
    state: any
    text: {
        label: string
        header: string
        body: string
        confirm: string
        abort: string
    }
}

export const AddDialog: React.FC<FormDialogProps> = ({
    handleCategorie,
    state,
    text
}) => {
    const [open, setOpen] = state
    const [textValue, setTextValue] = useState('')
    const [selectValue, setSelectValue] = useState<'snack' | 'activity'>(
        'snack'
    )

    const handleClose = () => {
        setTextValue('')
        setOpen(false)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
            disableBackdropClick={true}
        >
            <DialogTitle id="form-dialog-title">{text.header}</DialogTitle>
            <DialogContent>
                <DialogContentText>{text.body}</DialogContentText>
                <Select
                    style={{ minWidth: '100px' }}
                    value={selectValue}
                    onChange={(event) => {
                        setSelectValue(
                            event.target.value as 'snack' | 'activity'
                        )
                    }}
                >
                    <MenuItem value="snack">Snack</MenuItem>
                    <MenuItem value="activity">Aktivität</MenuItem>
                </Select>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label={text.label}
                    fullWidth
                    value={textValue}
                    onChange={(e) => setTextValue(e.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    {text.abort}
                </Button>
                <Button
                    onClick={() => {
                        handleClose()
                        handleCategorie(selectValue, textValue)
                    }}
                    color="primary"
                >
                    {text.confirm}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
