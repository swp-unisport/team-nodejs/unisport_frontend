import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import {
    MenuItem,
    Select,
    Table,
    TableBody,
    TableCell,
    TableRow
} from '@material-ui/core'
import { Language, TranslatedString } from '../../../../types/ApiTypes'
import Collapse from '@material-ui/core/Collapse'

interface FormDialogProps {
    handleCategorie: (
        text: { de: string; en: string; fr: string },
        title: string
    ) => void
    state: any
    textDialog: {
        labelOne: string
        labelTwo: string
        header: string
        body: string
        confirm: string
        abort: string
    }
    snackState: any
}

export const ChangeDialog: React.FC<FormDialogProps> = ({
    handleCategorie,
    state,
    textDialog,
    snackState
}) => {
    const [open, setOpen] = state
    const [textOneValue, setTextOneValue] = useState(snackState.title)
    useEffect(() => {
        setTextOneValue(snackState.title)
        setTextTwoValue(snackState.text)
    }, [snackState])
    /* console.log('snackState')
    console.log(snackState)
    console.log('textOneValue')
    console.log(textOneValue) */
    const [textTwoValue, setTextTwoValue] = useState(snackState.text)
    const languages = [Language.ENGLISH, Language.FRENCH]

    const handleClose = () => {
        setTextTwoValue({ de: '', en: '', fr: '' })
        setTextOneValue('')
        setOpen(false)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
            disableBackdropClick={true}
        >
            <DialogTitle id="form-dialog-title">
                {textDialog.header}
            </DialogTitle>
            <DialogContent>
                <DialogContentText>{textDialog.body}</DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="name"
                    label={textDialog.labelOne}
                    fullWidth
                    value={textOneValue}
                    onChange={(e) => setTextOneValue(e.target.value)}
                />
                <TextField
                    autoFocus
                    margin="dense"
                    variant="outlined"
                    id="name"
                    label={textDialog.labelTwo}
                    fullWidth
                    value={textTwoValue['de']}
                    onChange={(e) =>
                        setTextTwoValue({ ...textTwoValue, de: e.target.value })
                    }
                />
                <Table style={{ maxWidth: '700px' }}>
                    <TableBody>
                        {languages.map((entry: Language) => {
                            return (
                                <TableRow key={entry}>
                                    <TableCell>
                                        <TextField
                                            fullWidth
                                            id="outlined-basic"
                                            variant="outlined"
                                            label={entry}
                                            value={textTwoValue[entry]}
                                            onChange={(e) =>
                                                setTextTwoValue({
                                                    ...textTwoValue,
                                                    [entry]: e.target.value
                                                })
                                            }
                                        />
                                    </TableCell>
                                </TableRow>
                            )
                        })}
                    </TableBody>
                </Table>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    {textDialog.abort}
                </Button>
                <Button
                    onClick={() => {
                        handleClose()
                        handleCategorie(textTwoValue, textOneValue)
                    }}
                    color="primary"
                >
                    {textDialog.confirm}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
