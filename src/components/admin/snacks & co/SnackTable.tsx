import React, { useState } from 'react'
import {
    Button,
    createStyles,
    Dialog,
    DialogTitle,
    makeStyles,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Theme
} from '@material-ui/core'
import EditIcon from '@material-ui/icons/Edit'
import SearchIcon from '@material-ui/icons/Search'
import ClearIcon from '@material-ui/icons/Clear'
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import { ImageManagerPopup } from './ImageManager'
import { ChangeDialog } from './Dialogs/ChangeDialog'
import { TranslatedString } from '../../../types/ApiTypes'
import PhotoCameraIcon from '@material-ui/icons/PhotoCamera'

interface SnackTableProps {
    data: any
    setData: React.Dispatch<React.SetStateAction<any>>
    active: boolean
}

const defaultSnackInfo = {
    index: 0,
    title: '',
    text: { de: '', en: '', fr: '' },
    imageUrl: undefined
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        previewGrid: {
            display: 'grid',
            gridTemplateColumns: '1fr 1fr',
            maxWidth: '600px'
        },
        previewText: {
            padding: '6px'
        }
    })
)

export const SnackTable: React.FC<SnackTableProps> = ({
    data,
    setData,
    active
}) => {
    const classes = useStyles()
    const dialogChangeState = useState(false)
    const [dialogPreviewState, setDialogPreviewState] = useState(false)
    const [snackInfo, setSnackInfo] = useState({ ...defaultSnackInfo })

    function onImgPopUpClose(url: string | null, index: number) {
        if (url) handleChange(index, 'image', url)
        setShowImage(false)
    }

    function handleChange(
        index: number,
        mode: 'active' | 'delete' | 'image' | 'title' | 'text',
        text?: string,
        descriptions?: TranslatedString
    ) {
        let copy = [...data]
        if (mode == 'active') copy[index].active = !copy[index].active
        if (mode == 'delete') copy.splice(index, 1)
        if (text) {
            if (mode == 'image') {
                copy[index].imageUrl = text
                setSnackInfo({ ...defaultSnackInfo })
            }
            if (mode == 'title') copy[index].title = text
        }
        if (mode == 'text' && descriptions)
            copy[index].descriptions = descriptions

        console.log('snackTable onChange', copy)
        setData(copy)
    }

    const [showImage, setShowImage] = useState(false)

    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell> </TableCell>
                    <TableCell>Typ</TableCell>
                    <TableCell>Titel</TableCell>
                    <TableCell> </TableCell>
                    <TableCell> </TableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {data.map((entry: any, idx: number) => {
                    if (entry.active === active) {
                        return (
                            <TableRow key={idx}>
                                <TableCell align="left">
                                    <Button
                                        onClick={() => {
                                            setSnackInfo({
                                                index: idx,
                                                title: entry.title,
                                                text: entry.descriptions,
                                                imageUrl: entry.imageUrl
                                            })
                                            setShowImage(true)
                                        }}
                                    >
                                        <PhotoCameraIcon />
                                    </Button>
                                    <Button
                                        onClick={() => {
                                            setSnackInfo({
                                                index: idx,
                                                title: entry.title,
                                                text: entry.descriptions,
                                                imageUrl: entry.imageUrl
                                            })
                                            dialogChangeState[1](true)
                                        }}
                                    >
                                        <EditIcon />
                                    </Button>
                                </TableCell>
                                <TableCell align="left">{entry.type}</TableCell>
                                <TableCell align="left">
                                    {entry.title}
                                </TableCell>
                                <TableCell align="left">
                                    <Button
                                        onClick={() => {
                                            setSnackInfo({
                                                index: idx,
                                                title: entry.title,
                                                text: entry.descriptions,
                                                imageUrl: entry.imageUrl
                                            })
                                            setDialogPreviewState(true)
                                        }}
                                    >
                                        {/* TODO render preview of snack/activity*/}
                                        <SearchIcon />{' '}
                                    </Button>
                                </TableCell>
                                <TableCell>
                                    <Button
                                        onClick={() =>
                                            handleChange(idx, 'active')
                                        }
                                    >
                                        {entry.active && <ArrowDownwardIcon />}
                                        {!entry.active && <ArrowUpwardIcon />}
                                    </Button>
                                </TableCell>
                                <TableCell align="left">
                                    <Button
                                        onClick={() =>
                                            handleChange(idx, 'delete')
                                        }
                                    >
                                        {' '}
                                        <ClearIcon />{' '}
                                    </Button>
                                </TableCell>
                            </TableRow>
                        )
                    } else return null
                })}
            </TableBody>
            <ChangeDialog
                handleCategorie={(text: TranslatedString, title: string) => {
                    handleChange(snackInfo.index, 'title', title)
                    handleChange(snackInfo.index, 'text', '', text)
                }}
                state={dialogChangeState}
                textDialog={{
                    labelOne: 'Arbeitstitel',
                    labelTwo: 'Text De',
                    header: 'Text bearbeiten',
                    body: '',
                    confirm: 'Übernehmen',
                    abort: 'Abbrechen'
                }}
                snackState={snackInfo}
            />
            <ImageManagerPopup
                initialUrl={snackInfo.imageUrl}
                index={snackInfo.index}
                open={showImage}
                onClose={onImgPopUpClose}
            />
            <Dialog
                open={dialogPreviewState}
                onClose={() => {
                    setDialogPreviewState(false)
                    setSnackInfo({ ...defaultSnackInfo })
                }}
            >
                <DialogTitle>Preview für {snackInfo.title}</DialogTitle>
                <div className={classes.previewGrid}>
                    <img
                        src={snackInfo.imageUrl}
                        style={{ maxWidth: '100%' }}
                    ></img>
                    <div className={classes.previewText}>
                        {Object.entries(snackInfo.text).map(([lang, text]) => {
                            return (
                                <p key={lang}>
                                    {lang}: {text}
                                </p>
                            )
                        })}
                    </div>
                </div>
            </Dialog>
        </Table>
    )
}
