import React, { useState } from 'react'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import { TextField } from '@material-ui/core'

const LoginView: React.FC<{
    setLoggedIn: React.Dispatch<React.SetStateAction<boolean>>
}> = ({ setLoggedIn }) => {
    const [username, setUserName] = useState('')
    const [password, setPassword] = useState('')

    const useLogin = () => {
        async function login() {
            try {
                const token = await loginUser({
                    username,
                    password
                })
                localStorage.setItem('token', JSON.stringify(token))
                setLoggedIn(true)
            } catch (error) {
                alert(error.status + '\n' + error.message)
            }
        }
        return login()
    }

    return (
        <div id="login-view">
            <Card>
                <CardContent>
                    <form noValidate>
                        <TextField
                            id="standard-basic"
                            label="Username"
                            fullWidth
                            onChange={(e) => setUserName(e.target.value)}
                        />
                        <TextField
                            id="standard-basic"
                            label="Password"
                            fullWidth
                            onChange={(e) => setPassword(e.target.value)}
                            type="password"
                        />
                    </form>
                </CardContent>
                <CardActions style={{ justifyContent: 'end' }}>
                    <Button
                        variant="contained"
                        color="primary"
                        onClick={useLogin}
                    >
                        Login
                    </Button>
                </CardActions>
            </Card>
        </div>
    )
}

/**
 * @throws {{status: string, message: string}} - Will throw if the user is not logged in
 */
async function loginUser(credentials: { username: string; password: string }) {
    const res = await fetch('/api/auth/signin', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(credentials)
    })

    if (res.status !== 200) {
        throw { status: res.statusText, message: (await res.json()).message }
    }

    return res.json()
}

export default LoginView
