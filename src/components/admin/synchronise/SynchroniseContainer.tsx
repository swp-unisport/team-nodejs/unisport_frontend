import React from 'react'
import { Box, makeStyles } from '@material-ui/core'
import { SynchroniseCard } from './SynchroniseCard'
import { createStyles, Theme } from '@material-ui/core/styles'
import { SyncListEntry } from './SynchroniseView'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        div: {
            alignItems: 'center',
            display: 'inline-flex',
            padding: '5px'
        },
        greenButton: {
            background: '#6B9E1E',
            width: 15,
            height: 15,
            margin: '5px'
        },
        redButton: {
            background: '#c42727',
            width: 15,
            height: 15,
            margin: '5px'
        },
        p: {
            marginRight: '50px'
        }
    })
)

interface SynchroniseContainerProps {
    sports: SyncListEntry[]
    text: [string, string, string]
    sportsRef: React.MutableRefObject<SyncListEntry[]>
}

export const SynchroniseContainer: React.FC<SynchroniseContainerProps> = ({
    sports,
    sportsRef,
    text
}) => {
    const classes = useStyles()
    return (
        <Box
            border={1}
            borderRadius="borderRadius"
            overflow="auto"
            maxHeight="550px"
            borderColor="DarkGrey"
            m={5}
        >
            <div className={classes.div}>
                <p className={classes.p}>{text[0]}</p>
                <div className={classes.redButton} />
                <p>{text[1]}</p>
                <div className={classes.greenButton} />
                <p>{text[2]}</p>
            </div>
            {sports.map((sport, idx) => {
                return (
                    <SynchroniseCard
                        key={sport.name}
                        titel={sport.name}
                        active={true}
                        sportsRef={sportsRef}
                        index={idx}
                    />
                )
            })}
        </Box>
    )
}
