import React, { useState } from 'react'
import { makeStyles, Paper } from '@material-ui/core'
import { createStyles, Theme } from '@material-ui/core/styles'
import { SyncListEntry } from './SynchroniseView'

type SynchroniseCardProp = {
    titel: string
    active: boolean
    index: number
    sportsRef: React.MutableRefObject<SyncListEntry[]>
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        green: {
            background: '#6B9E1E',
            textAlign: 'center',
            margin: '15px'
        },
        red: {
            background: '#c42727',
            textAlign: 'center',
            margin: '15px'
        }
    })
)

export const SynchroniseCard: React.FC<SynchroniseCardProp> = ({
    titel,
    active,
    sportsRef,
    index
}) => {
    const classes = useStyles()
    const [isActive, setIsActive] = useState(active)

    const toggleActive = () => {
        setIsActive(!isActive)
        sportsRef.current[index].selected = !isActive
    }

    if (isActive)
        return (
            <Paper onClick={toggleActive} className={classes.green}>
                {titel}
            </Paper>
        )
    else
        return (
            <Paper onClick={toggleActive} className={classes.red}>
                {titel}
            </Paper>
        )
}
