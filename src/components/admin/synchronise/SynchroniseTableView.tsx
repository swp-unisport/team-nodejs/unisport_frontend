import React, { useState } from 'react'
import { Fab, makeStyles } from '@material-ui/core'
import SaveIcon from '@material-ui/icons/Save'
import { createStyles, Theme } from '@material-ui/core/styles'
import { SynchroniseContainer } from './SynchroniseContainer'
import { SyncListEntry } from './SynchroniseView'
import { useRef } from 'react'
import { ConfirmDialog } from '../shared/Dialogs/ConfirmDialog'
import { adminFetch } from '../../../utils/common'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        fab: {
            position: 'fixed',
            bottom: theme.spacing(4),
            right: theme.spacing(4)
        },
        div: {
            justifyContent: 'center',
            alignItems: 'center',
            display: 'flex',
            flexDirection: 'row',
            width: '100%',
            minHeight: '100%',
            paddingTop: '100px'
        }
    })
)

type SynchroniseTableViewProps = {
    removedSports: SyncListEntry[]
    recurringSports: (SyncListEntry & { lastActive: boolean })[]
    newSports: SyncListEntry[]
}

export const SynchroniseTableView: React.FC<SynchroniseTableViewProps> = ({
    recurringSports,
    removedSports,
    newSports
}) => {
    const classes = useStyles()
    const recurringSportsRef = useRef(recurringSports)
    const removedSportsRef = useRef(removedSports)
    const newSportsRef = useRef(newSports)
    const dialogConfirmState = useState(false)

    async function saveToDB(
        recR: React.MutableRefObject<
            (SyncListEntry & { lastActive: boolean })[]
        >,
        remR: React.MutableRefObject<SyncListEntry[]>,
        newR: React.MutableRefObject<SyncListEntry[]>
    ) {
        let toRemove: { name: string; url: string }[] = []
        let toAdd: { name: string; url: string }[] = []
        let toActive: { name: string; url: string }[] = []
        let toInactive: { name: string; url: string }[] = []
        remR.current.forEach((entry) => {
            const { selected, ...rest } = entry
            if (!selected) {
                toRemove.push(rest)
            } else {
                toInactive.push(rest)
            }
        })
        newR.current.forEach((entry) => {
            if (entry.selected) {
                const { selected, ...rest } = entry
                toAdd.push(rest)
            }
        })
        recR.current.forEach((entry) => {
            const { selected, lastActive, ...rest } = entry
            if (selected) {
                toActive.push(rest)
            } else {
                toInactive.push(rest)
            }
        })

        const res = await adminFetch('/api/sports', {
            method: 'POST',
            body: JSON.stringify({
                toRemove,
                toAdd,
                toActive,
                toInactive
            }),
            headers: { 'Content-Type': 'application/json' }
        })

        if (res.status !== 200) {
            const t = await res.text()
            console.log(t)
            alert('Etwas ist schiefgelaufen: ' + t)
        }
    }

    return (
        <div style={{ minHeight: '100%', textAlign: 'center' }}>
            <div className={classes.div}>
                <SynchroniseContainer
                    sports={newSports}
                    text={['neue Sportarten', 'nicht übernehmen', 'übernehmen']}
                    sportsRef={newSportsRef}
                />
                <SynchroniseContainer
                    sports={recurringSports}
                    text={[
                        'wiedererkannte Sportarten',
                        'nicht übernehmen',
                        'übernehmen'
                    ]}
                    sportsRef={recurringSportsRef}
                />
                <SynchroniseContainer
                    sports={removedSports}
                    text={[
                        'archivierte Sportarten',
                        'nicht archivieren',
                        'archivieren'
                    ]}
                    sportsRef={removedSportsRef}
                />
                <ConfirmDialog
                    handleCategorie={() =>
                        saveToDB(
                            recurringSportsRef,
                            removedSportsRef,
                            newSportsRef
                        )
                    }
                    state={dialogConfirmState}
                    text={{
                        header: 'Speichern',
                        body: 'Sollen die Änderungen gespeichert werden',
                        confirm: 'Bestätigen',
                        abort: 'Abbrechen'
                    }}
                />
            </div>

            <Fab
                className={classes.fab}
                onClick={() => dialogConfirmState[1](true)}
            >
                <SaveIcon />
            </Fab>
        </div>
    )
}
