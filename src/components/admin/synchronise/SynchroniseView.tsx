import React, { useState } from 'react'
import { Button } from '@material-ui/core'
import { SynchroniseTableView } from './SynchroniseTableView'

export type SyncApiData = {
    newSports: { name: string; url: string }[]
    recurringSports: { name: string; url: string; lastActive: boolean }[]
    removedSports: { name: string; url: string }[]
}

export interface SyncListEntry {
    name: string
    url: string
    selected: boolean
}

export const SynchroniseView: React.FC = () => {
    const [mode, setMode] = useState<'start' | 'loading' | 'finished'>('start')

    const [fetchedData, setFetchedData] = useState<SyncApiData>({
        newSports: [],
        recurringSports: [],
        removedSports: []
    })

    const loadData = async () => {
        setMode('loading')
        const res = await fetch('/api/sync')

        if (res.status !== 200) {
            console.error('Failed to call /api/sync', await res.text())
            alert('Fehler beim Synchonisieren.')
            return
        }

        const data: SyncApiData = await res.json()
        if (!data.newSports || !data.recurringSports || !data.removedSports) {
            console.error('response not in expected format')
            return
        }

        setFetchedData(data)

        setMode('finished')
    }

    const toSyncListEntry = <T extends SyncApiData['newSports'][number]>(
        e: T
    ): T & SyncListEntry => {
        const a = (e as unknown) as SyncListEntry
        a.selected = true
        return a as T & SyncListEntry
    }

    return (
        <div>
            {mode === 'start' && (
                <div style={{ textAlign: 'center', paddingTop: '300px' }}>
                    <Button onClick={() => loadData()}>Synchronisiere</Button>
                </div>
            )}
            {mode === 'loading' && <h1>Warte auf Server...</h1>}
            {mode === 'finished' && (
                <SynchroniseTableView
                    newSports={fetchedData.newSports.map(toSyncListEntry)}
                    recurringSports={fetchedData.recurringSports.map(
                        toSyncListEntry
                    )}
                    removedSports={fetchedData.removedSports.map(
                        toSyncListEntry
                    )}
                />
            )}
        </div>
    )
}
