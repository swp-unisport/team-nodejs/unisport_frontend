import React, { useState } from 'react'
import SpeedDialAction from '@material-ui/lab/SpeedDialAction'
import MoreVertIcon from '@material-ui/icons/MoreVert'
import SpeedDial from '@material-ui/lab/SpeedDial'
import { makeStyles } from '@material-ui/core'

export interface action {
    icon: JSX.Element
    title: string
    handler: () => void
}

interface SpeedDialProps {
    actions: action[]
}

const useStyles = makeStyles((theme) => ({
    fab: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2)
    }
}))

export const CustomSpeedDial: React.FC<SpeedDialProps> = ({ actions }) => {
    const classes = useStyles()
    const [open, setOpen] = useState(false)
    return (
        <SpeedDial
            className={classes.fab}
            icon={<MoreVertIcon />}
            open={open}
            onClick={() => setOpen(!open)}
            ariaLabel="SpeedDial"
        >
            {actions.map((action) => {
                return (
                    <SpeedDialAction
                        key={action.title}
                        icon={action.icon}
                        onClick={action.handler}
                        title={action.title}
                    />
                )
            })}
        </SpeedDial>
    )
}
