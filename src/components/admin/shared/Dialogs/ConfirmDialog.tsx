import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

interface FormDialogProps {
    handleCategorie: () => void
    state: any
    text: {
        header: string
        body: string
        confirm: string
        abort: string
    }
}

export const ConfirmDialog: React.FC<FormDialogProps> = ({
    handleCategorie,
    state,
    text
}) => {
    const [open, setOpen] = state

    const handleClose = () => {
        setOpen(false)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
            disableBackdropClick={true}
        >
            <DialogTitle id="form-dialog-title">{text.header}</DialogTitle>
            <DialogContent>
                <DialogContentText>{text.body}</DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    {text.abort}
                </Button>
                <Button
                    onClick={() => {
                        handleCategorie()
                        handleClose()
                    }}
                    color="primary"
                >
                    {text.confirm}
                </Button>
            </DialogActions>
        </Dialog>
    )
}
