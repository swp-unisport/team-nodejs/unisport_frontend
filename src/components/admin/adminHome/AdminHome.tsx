import React, { useState } from 'react'
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom'
import { useHistory } from 'react-router-dom'

import { OurDrawer, drawerWidth } from '../navigation/Drawer'
import { QuestionView } from '../questions/QuestionView'
import { SnacksView } from '../snacks & co/SnacksView'
import LoginView from '../loginView/LoginView'
import Statistik from '../statistik/Statistik'

import {
    AppBar,
    Button,
    createStyles,
    makeStyles,
    Theme,
    Toolbar,
    Typography
} from '@material-ui/core'

import { TranslationView } from '../translation/TranslationView'
import { SportartenView } from '../sports/SportartenView'
import { LocalDiningOutlined } from '@material-ui/icons'
import { SynchroniseView } from '../synchronise/SynchroniseView'
import { OrderView } from '../order/OrderView'
import { useFetchData } from '../useFetchData'
import jwtDecode from 'jwt-decode'

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex'
        },
        appBar: {
            zIndex: theme.zIndex.drawer + 1
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0
        },
        drawerPaper: {
            width: drawerWidth
        },
        drawerContainer: {
            overflow: 'auto'
        },
        content: {
            flexGrow: 1,
            padding: 0,
            display: 'grid',
            gridTemplateRows: 'min-content auto'
        },
        header: {
            display: 'flex',
            justifyContent: 'space-between'
        },
        login: {
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
        }
    })
)

const AdminHome: React.FC = () => {
    const classes = useStyles()
    let { path } = useRouteMatch()

    const [loggedIn, setLoggedIn] = useState(false)
    useFetchData()

    const logout = () => {
        localStorage.removeItem('token')
        setLoggedIn(false)
    }

    const token = localStorage.getItem('token')

    const tokenExpiry = token && jwtDecode<{ exp: number }>(token)?.exp

    if (!token || !tokenExpiry || tokenExpiry <= Date.now() / 1000) {
        console.log(tokenExpiry, tokenExpiry! <= Date.now() / 1000)
        localStorage.removeItem('token')
        if (loggedIn) setLoggedIn(false)
        return (
            <div className={`${classes.login}`} style={{ minHeight: '100vh' }}>
                <LoginView setLoggedIn={setLoggedIn} />
            </div>
        )
    }

    return (
        <div className={`${classes.root}`} style={{ minHeight: '100vh' }}>
            <OurDrawer />
            <main className={`${classes.content}`}>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar className={classes.header}>
                        <Typography variant="h6">News</Typography>
                        <Button color="inherit" onClick={logout}>
                            Logout
                        </Button>
                    </Toolbar>
                </AppBar>
                <Toolbar />
                <Switch>
                    <Route path={`${path}/sportarten`}>
                        <SportartenView></SportartenView>
                    </Route>
                    <Route path={`${path}/statistiken`}>
                        <Statistik></Statistik>
                    </Route>

                    <Route path={`${path}/fragen`}>
                        <QuestionView />
                    </Route>
                    <Route path={`${path}/synchronisieren`}>
                        <SynchroniseView />
                    </Route>
                    <Route path={`${path}/snacks`}>
                        <SnacksView />
                    </Route>
                    <Route path={`${path}/reinfolge`}>
                        <OrderView />
                    </Route>
                    <Route path={`${path}/translation`}>
                        <TranslationView />
                    </Route>
                    {/* <Route path={`${path}/login`}>
						<LoginView />
					</Route> */}
                    <Route path={'*'}>
                        <Redirect to="/admin/sportarten" />
                    </Route>
                </Switch>
            </main>
        </div>
    )
}

export default AdminHome
