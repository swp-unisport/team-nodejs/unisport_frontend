import { ISports } from '../../../types/ApiTypes'

export function compareSportWeights(a: ISports, b: ISports) {
    let c1 = 0
    let c2 = 0
    Object.keys(a.categoryWeights).forEach((key) => {
        if (a.active) c1 += 100
        if (a.categoryWeights[key] === null) {
            c1 += 1
        }
    })
    Object.keys(b.categoryWeights).forEach((key) => {
        if (b.active) c2 += 100
        if (b.categoryWeights[key] === null) {
            c2 += 1
        }
    })
    if (c1 > c2) return -1
    else if (c1 < c2) return 1
    return 0
}
