import React, { useRef } from 'react'
import {
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow
} from '@material-ui/core'
import { ArrowDownward, ArrowUpward } from '@material-ui/icons'
import ClearIcon from '@material-ui/icons/Clear'
import { ICategorie } from '../shared/ICategorie'
import { CustomCell } from './SportartenCustomCell'
import { ISports } from '../../../types/ApiTypes'
import { useState } from 'react'
import { useEffect } from 'react'
import { CustomTableRow } from './SportartenRow'

interface SportartenTableProps {
    categories: string[]
    sportarten: ISports[]
    sportartenRef: React.MutableRefObject<ISports[]>
    activ: boolean
    updateRef: any
}

export const SportartenTable: React.FC<SportartenTableProps> = ({
    sportarten,
    categories,
    sportartenRef,
    activ,
    updateRef
}: SportartenTableProps) => {
    return (
        <Table>
            <TableHead>
                <TableRow>
                    <TableCell> Sportarten </TableCell>
                    {categories.map((elem) => {
                        return <TableCell key={elem}>{elem}</TableCell>
                    })}
                    <TableCell />
                    <TableCell>URL</TableCell>
                    <TableCell />
                    <TableCell />
                </TableRow>
            </TableHead>
            <TableBody>
                {sportarten.map((elem, idx) => {
                    if (elem.active !== activ) return null
                    return (
                        <CustomTableRow
                            key={elem._id}
                            sportId={elem._id}
                            index={idx}
                            valueRef={sportartenRef}
                            updateRef={updateRef}
                            sportarten={sportarten}
                        />
                    )
                })}
            </TableBody>
        </Table>
    )
}
