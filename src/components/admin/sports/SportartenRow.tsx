import React from 'react'
import { IconButton, TableCell, TableRow } from '@material-ui/core'
import { CustomCell } from './SportartenCustomCell'
import { ArrowDownward, ArrowUpward } from '@material-ui/icons'
import ClearIcon from '@material-ui/icons/Clear'
import { ISports } from '../../../types/ApiTypes'

interface CustomTableRowProps {
    index: number
    valueRef: React.MutableRefObject<ISports[]>
    updateRef: any
    sportarten: ISports[]
    sportId: string
}

export const CustomTableRow: React.FC<CustomTableRowProps> = ({
    index,
    valueRef,
    updateRef,
    sportarten,
    sportId
}) => {
    const sport = valueRef.current[index]
    console.log('rerender sport row ' + index)
    return (
        <TableRow>
            <TableCell> {sport.name}</TableCell>
            {Object.keys(sport.categoryWeights).map(function (key) {
                return (
                    <TableCell key={sport.name + '_' + key}>
                        <CustomCell
                            index={index}
                            categorieId={key}
                            valueRef={valueRef}
                            data={sport.categoryWeights[key]}
                            sportarten={sportarten}
                        />
                    </TableCell>
                )
            })}
            <TableCell />
            <TableCell>
                <a
                    href={'https://' + sport.url}
                    target="_blank"
                    rel="noreferrer"
                >
                    {sport.url}
                </a>
            </TableCell>
            <TableCell>
                <IconButton
                    onClick={() => {
                        sport.active = !sport.active
                        updateRef()
                    }}
                >
                    {sport.active ? <ArrowDownward /> : <ArrowUpward />}
                </IconButton>
            </TableCell>
            <TableCell>
                <IconButton
                    onClick={() => {
                        valueRef.current.splice(index, 1)
                        updateRef()
                    }}
                >
                    <ClearIcon />
                </IconButton>
            </TableCell>
        </TableRow>
    )
}
