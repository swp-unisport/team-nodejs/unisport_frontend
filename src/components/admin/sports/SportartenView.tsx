import React, { useEffect, useRef, useState } from 'react'
import SaveIcon from '@material-ui/icons/Save'
import { SportartenTable } from './SportartenTable'
import FileCopyIcon from '@material-ui/icons/FileCopy'
import { CustomSpeedDial } from '../shared/CustomSpeedDial'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import { DeleteDialog } from './Dialogs/DeleteDialog'
import { AddDialog } from '../shared/Dialogs/AddDialog'
import { CopyDialog } from './Dialogs/CopyDialog'
import { ISports } from '../../../types/ApiTypes'
import SortIcon from '@material-ui/icons/Sort'
import { compareSportWeights } from './compareSportsWeights'
import ObjectID from 'bson-objectid'
import { useAppDispatch, useAppSelector } from '../../../hooks'
import { ConfirmDialog } from '../shared/Dialogs/ConfirmDialog'
import { updateSports } from '../../../redux/Slices/sportsSlice'
import { updateCategoriesFromSport } from '../../../redux/Slices/categoriesSlice'

export const SportartenView: React.FC = () => {
    const [categories, setCategories] = useState<string[]>([])
    const [sportarten, setSportarten] = useState<ISports[]>([])
    const data = useAppSelector((state) => state.sportsData.value) as ISports[]

    useEffect(() => {
        setSportarten(data)
        sportartenRef.current = JSON.parse(JSON.stringify(data))
        if (data.length) setCategories(Object.keys(data[0].categoryWeights))
    }, [data])

    const sportartenRef = useRef(sportarten)
    const dialogAddState = useState(false)
    const dialogDeleteState = useState(false)
    const dialogCopyState = useState(false)
    const dialogConfirmState = useState(false)
    const dispatch = useAppDispatch()

    async function saveSportsToDB(sports: React.MutableRefObject<ISports[]>) {
        const res = await fetch('/api/sports', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(sports.current)
        }).then((e) => {
            if (e.ok) {
                dispatch(updateSports(sports.current))
                dispatch(updateCategoriesFromSport(sports.current[0]))
            }
        })
    }

    function handleCategorieAdd(title: string) {
        let copy = [...categories]
        let copy2 = sportarten.map((s) => Object.assign({}, s))
        if (title === '') {
            return
        }
        const categorieId = new ObjectID().toHexString()
        copy.push(title)
        copy2.forEach((sport) => {
            sport.categoryWeights = {
                ...sport.categoryWeights,
                [categorieId]: null
            }
        })
        sportartenRef.current = JSON.parse(JSON.stringify(copy2))
        setCategories(copy)
        setSportarten(copy2)
    }

    function handleCategorieDelete(categorieId: string) {
        let copy = [...categories]
        let copy2 = [...sportarten]
        let index = -1
        copy.forEach((categorie, idx) => {
            if (categorie === categorieId) {
                index = idx
            }
        })
        if (index >= 0) {
            copy.splice(index, 1)
            copy2.forEach((sport) => {
                delete sport.categoryWeights[categorieId]
            })
        }
        sportartenRef.current = JSON.parse(JSON.stringify(copy2))
        setCategories(copy)
        setSportarten(copy2)
    }

    function handleSportCopy(sport1: string, sport2: string) {
        let copy = [...sportarten]
        let idx1 = -1
        let idx2 = -1
        copy.forEach((sport, idx) => {
            if (sport._id === sport1) {
                idx1 = idx
            }
            if (sport._id === sport2) {
                idx2 = idx
            }
        })
        if (idx1 === -1 || idx2 === -1) {
            return
        } else {
            copy[idx2].categoryWeights = copy[idx1].categoryWeights
        }
        sportartenRef.current = JSON.parse(JSON.stringify(copy))
        setSportarten(copy)
    }

    const actions = [
        {
            icon: <SortIcon />,
            title: 'Sortieren',
            handler: () => {
                let copy = [...sportartenRef.current]
                const t = copy.sort(compareSportWeights)
                setSportarten(t)
                sportartenRef.current = t
            }
        },
        {
            icon: <SaveIcon />,
            title: 'Speichern',
            handler: () => dialogConfirmState[1](true)
        },
        {
            icon: <FileCopyIcon />,
            title: 'Kopieren',
            handler: () => dialogCopyState[1](true)
        },
        {
            icon: <DeleteIcon />,
            title: 'Löschen',
            handler: () => dialogDeleteState[1](true)
        },
        {
            icon: <AddIcon />,
            title: 'Hinzufügen',
            handler: () => dialogAddState[1](true)
        }
    ]

    return (
        <div>
            <h2>Aktive Sportarten</h2>
            <SportartenTable
                updateRef={() => setSportarten([...sportartenRef.current])}
                sportartenRef={sportartenRef}
                categories={categories}
                sportarten={sportarten}
                activ={true}
            />
            <h2>Inaktive Sportarten</h2>
            <SportartenTable
                updateRef={() => setSportarten([...sportartenRef.current])}
                sportartenRef={sportartenRef}
                categories={categories}
                sportarten={sportarten}
                activ={false}
            />
            <CustomSpeedDial actions={actions} />
            <DeleteDialog
                categories={categories}
                handleCategorie={handleCategorieDelete}
                state={dialogDeleteState}
            />
            <AddDialog
                text={{
                    label: 'Kategorie',
                    header: 'Kategorie hinzufügen',
                    body: 'Wie soll die neue Kategorie heißen?',
                    confirm: 'Bestätigen',
                    abort: 'Abbrechen'
                }}
                handleCategorie={handleCategorieAdd}
                state={dialogAddState}
            />
            <CopyDialog
                sportarten={sportarten}
                handleSportCopy={handleSportCopy}
                state={dialogCopyState}
            />
            <ConfirmDialog
                handleCategorie={() => saveSportsToDB(sportartenRef)}
                state={dialogConfirmState}
                text={{
                    header: 'Speichern',
                    body: 'Sollen die Änderungen gespeichert werden',
                    confirm: 'Bestätigen',
                    abort: 'Abbrechen'
                }}
            />
        </div>
    )
}
