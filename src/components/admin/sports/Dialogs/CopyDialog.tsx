import React, { useState } from 'react'
import {
    Button,
    Checkbox,
    Dialog,
    DialogTitle,
    List,
    ListItem,
    ListItemIcon,
    ListItemText
} from '@material-ui/core'
import DialogContent from '@material-ui/core/DialogContent'
import DialogActions from '@material-ui/core/DialogActions'
import { ISports } from '../../../../types/ApiTypes'

interface CopyDialogProps {
    sportarten: ISports[]
    handleSportCopy: (categorie1: string, categorie2: string) => void
    state: any
}

export const CopyDialog: React.FC<CopyDialogProps> = ({
    sportarten,
    handleSportCopy,
    state
}) => {
    const [open, setOpen] = state
    const [firstSelected, setFirstSelected] = useState('')
    const [secondSelected, setSecondSelected] = useState('')
    function handleClose() {
        setOpen(false)
        setFirstSelected('')
        setSecondSelected('')
    }

    return (
        <Dialog open={open} onClose={handleClose} disableBackdropClick={true}>
            <DialogTitle>Kopiere Werte</DialogTitle>
            <DialogContent>
                <div style={{ display: 'flex', alignItems: 'center' }}>
                    <h3>Von</h3>
                    <List>
                        {sportarten.map((sport) => {
                            return (
                                <ListItem key={sport._id}>
                                    <ListItemIcon>
                                        <Checkbox
                                            color="primary"
                                            onClick={() =>
                                                setFirstSelected(sport._id)
                                            }
                                            checked={
                                                firstSelected === sport._id
                                            }
                                        />
                                    </ListItemIcon>
                                    <ListItemText>{sport.name}</ListItemText>
                                </ListItem>
                            )
                        })}
                    </List>
                    <h3>Nach</h3>
                    <List>
                        {sportarten.map((sport) => {
                            return (
                                <ListItem key={sport._id}>
                                    <ListItemIcon>
                                        <Checkbox
                                            color="primary"
                                            onClick={() =>
                                                setSecondSelected(sport._id)
                                            }
                                            checked={
                                                secondSelected === sport._id
                                            }
                                        />
                                    </ListItemIcon>
                                    <ListItemText>{sport.name}</ListItemText>
                                </ListItem>
                            )
                        })}
                    </List>
                </div>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Abbrechen
                    </Button>
                    <Button
                        onClick={() => {
                            handleClose()
                            handleSportCopy(firstSelected, secondSelected)
                        }}
                        color="primary"
                    >
                        Bestätigen
                    </Button>
                </DialogActions>
            </DialogContent>
        </Dialog>
    )
}
