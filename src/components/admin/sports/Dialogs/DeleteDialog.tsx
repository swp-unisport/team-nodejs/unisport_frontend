import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { MenuItem, Select } from '@material-ui/core'
import { ICategorie } from '../../shared/ICategorie'

interface FormDialogProps {
    categories: string[]
    handleCategorie: (title: string) => void
    state: any
}

export const DeleteDialog: React.FC<FormDialogProps> = ({
    handleCategorie,
    categories,
    state
}) => {
    const [open, setOpen] = state
    const [value, setValue] = useState('')

    const handleClose = () => {
        setValue('')
        setOpen(false)
    }

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            aria-labelledby="form-dialog-title"
            disableBackdropClick={true}
        >
            <DialogTitle id="form-dialog-title">Kategorie löschen</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    Welche Kategorie soll gelöscht werden
                </DialogContentText>
                <Select
                    autoFocus
                    fullWidth
                    value={value}
                    onChange={(e) => {
                        setValue(e.target.value as string)
                    }}
                >
                    {categories.map((categorie) => {
                        return (
                            <MenuItem value={categorie} key={categorie}>
                                {categorie}
                            </MenuItem>
                        )
                    })}
                </Select>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose} color="primary">
                    Abbrechen
                </Button>
                <Button
                    onClick={() => {
                        handleClose()
                        handleCategorie(value)
                    }}
                    color="primary"
                >
                    Bestätigen
                </Button>
            </DialogActions>
        </Dialog>
    )
}
