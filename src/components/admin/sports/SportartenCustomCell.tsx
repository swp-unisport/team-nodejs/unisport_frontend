import React, { useEffect, useState } from 'react'
import { TextField } from '@material-ui/core'
import { ISports } from '../../../types/ApiTypes'

export const CustomCell: React.FC<{
    index: number
    categorieId: string
    valueRef: React.MutableRefObject<ISports[]>
    data: number | null
    sportarten: ISports[]
}> = ({ index, categorieId, valueRef, sportarten }) => {
    const [value, setValue] = useState<number | null>(
        sportarten[index].categoryWeights[categorieId]
    )
    useEffect(() => {
        setValue(sportarten[index].categoryWeights[categorieId])
    }, [sportarten, categorieId, index])
    const isError = value != null ? !(value >= 0) : true
    return (
        <TextField
            error={isError}
            type="number"
            value={isError ? '' : value}
            onChange={(e) => {
                setValue(parseInt(e.target.value, 10))
                if (e.target.value === '') {
                    valueRef.current[index].categoryWeights[categorieId] = null
                } else {
                    valueRef.current[index].categoryWeights[
                        categorieId
                    ] = parseInt(e.target.value, 10)
                }
            }}
        />
    )
}
