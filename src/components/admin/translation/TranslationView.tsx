import React, { useState } from 'react'
import {
    ListItem,
    List,
    makeStyles,
    ListItemText,
    Typography,
    Paper,
    Collapse,
    TextField
} from '@material-ui/core'
import { AppTranslations, Language } from '../../../types/ApiTypes'
import { ExpandLess, ExpandMore } from '@material-ui/icons'

const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4)
    },
    marginLeft: {
        marginLeft: theme.spacing(4)
    }
}))

export const TranslationView: React.FC = () => {
    const classes = useStyles()

    const translationObj: AppTranslations = {
        key1: {
            de: 'Hallo Welt',
            en: 'Hello World',
            fr: 'Bonjour le monde'
        }
    }

    const [expandedKeyState, setExpandendKeyState] = useState<
        { [key in keyof AppTranslations]?: boolean }
    >({})

    const toggleExpanded = (key: keyof AppTranslations): void => {
        const oldState = expandedKeyState[key] ?? false
        setExpandendKeyState({
            [key]: !oldState
        })
    }

    const TranslationList = (props: { appKey: keyof AppTranslations }) => {
        return (
            <React.Fragment>
                {Object.entries(Language).map(([langKey, langAbbr]) => (
                    <ListItem
                        button
                        className={classes.nested}
                        key={`${props.appKey}-${langKey}`}
                    >
                        <Typography style={{ width: '40px', display: 'block' }}>
                            {langAbbr}
                        </Typography>
                        <TextField fullWidth />
                    </ListItem>
                ))}
            </React.Fragment>
        )
    }

    return (
        <div>
            <Typography className={classes.marginLeft} variant="h2">
                Übersetzungen anpassen
            </Typography>
            <Paper
                variant="outlined"
                className={`${classes.marginLeft}`}
                style={{ maxWidth: '600px' }}
            >
                <List>
                    {(Object.entries(translationObj) as [
                        keyof AppTranslations,
                        string
                    ][]).map(([key, translatedString]) => (
                        <React.Fragment key={`${key}`}>
                            <ListItem
                                button
                                onClick={() => toggleExpanded(key)}
                            >
                                <ListItemText primary={key} />
                                {expandedKeyState[key] ? (
                                    <ExpandLess />
                                ) : (
                                    <ExpandMore />
                                )}
                            </ListItem>
                            <Collapse
                                in={expandedKeyState[key]}
                                timeout="auto"
                                unmountOnExit
                            >
                                <List component="div" disablePadding>
                                    <TranslationList appKey={key} />
                                </List>
                            </Collapse>
                        </React.Fragment>
                    ))}
                </List>
            </Paper>
        </div>
    )
}
