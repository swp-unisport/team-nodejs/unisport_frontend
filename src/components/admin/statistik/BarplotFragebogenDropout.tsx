import React from 'react'
import {
    BarChart,
    CartesianGrid,
    XAxis,
    YAxis,
    Bar,
    Tooltip,
    ResponsiveContainer
} from 'recharts'

const data1 = [
    { name: 'Snack2', relativ: 1 },
    { name: 'Frage1', relativ: 0.99 },
    { name: 'Frage2', relativ: 0.94 },
    { name: 'Frage3', relativ: 0.9 },
    { name: 'Frage4', relativ: 0.85 },
    { name: 'Frage5', relativ: 0.76 },
    { name: 'Frage6', relativ: 0.7 },
    { name: 'Snack7', relativ: 0.3 },
    { name: 'Snack8', relativ: 0.3 },
    { name: 'Snack9', relativ: 0.3 },
    { name: 'Snack10', relativ: 0.3 },
    { name: 'Snack2', relativ: 0.3 },
    { name: 'Snack3', relativ: 0.3 }
]

const BarplotFragebogenDropout: React.FC = () => {
    return (
        <div id="statistik-view">
            <h2 style={{ textAlign: 'center' }}> Fragebogenabbruch </h2>
            <div style={{ height: 400, paddingLeft: 50, display: 'flex' }}>
                <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                        margin={{ top: 20, right: 50, left: 25, bottom: 0 }}
                        data={data1}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" padding={{ left: 10 }} />
                        <YAxis
                            label={{
                                value:
                                    '% beantwortete pro Fragenbogen (gleiche Reihenfolge) oder % Abbruch bei dieser Frage',
                                offset: 5,
                                angle: -90,
                                position: 'insideBottomLeft'
                            }}
                            padding={{ bottom: 10 }}
                        />
                        <Tooltip />
                        <Bar dataKey="relativ" fill="#6B9E1F" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </div>
    )
}

export default BarplotFragebogenDropout
