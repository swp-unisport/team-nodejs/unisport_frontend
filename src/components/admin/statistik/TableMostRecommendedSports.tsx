import React from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableContainer from '@material-ui/core/TableContainer'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'

const TableMostRecommendedSports: React.FC = () => {
    const useStyles = makeStyles((theme: Theme) =>
        createStyles({
            table: {
                marginLeft: 25,
                marginRight: 50,
                maxWidth: '95%',
                position: 'relative'
            }
        })
    )

    function createData(
        index: number,
        name: string,
        relativ: string,
        total: string
    ) {
        return { index, name, relativ, total }
    }

    const rows = [
        createData(1, 'Schwimmen', '15%', '400'),
        createData(2, 'Klettern', '12%', '200'),
        createData(3, 'Fitness', '2%', '50')
    ]

    const classes = useStyles()

    return (
        <div id="statistik-view">
            <h2 style={{ textAlign: 'center' }}>
                Top vorgeschlagene Sportarten
            </h2>
            <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                    <TableHead>
                        <TableRow>
                            <TableCell> index </TableCell>
                            <TableCell>Sportarten</TableCell>
                            <TableCell align="right">relativ</TableCell>
                            <TableCell align="right">total</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {rows.map((row) => (
                            <TableRow key={row.index}>
                                <TableCell component="th" scope="row">
                                    {' '}
                                    {row.index}{' '}
                                </TableCell>
                                <TableCell>{row.name}</TableCell>
                                <TableCell align="right">
                                    {row.relativ}
                                </TableCell>
                                <TableCell align="right">{row.total}</TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        </div>
    )
}

export default TableMostRecommendedSports
