import React from 'react'
import BarplotSportsClicks from './BarplotSportsClicks'
import BarplotFragebogenDropout from './BarplotFragebogenDropout'
import TableMostRecommendedSports1 from './TableMostRecommendedSports1'

const Statistik: React.FC = () => {
    return (
        <div id="statistik-view">
            <BarplotSportsClicks />
            <div id="MostRecommended">
                <TableMostRecommendedSports1 />
            </div>
            <div id="FragenbogenDroput">
                <BarplotFragebogenDropout />
            </div>
        </div>
    )
}

export default Statistik
