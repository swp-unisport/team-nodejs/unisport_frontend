import React from 'react'

import {
    BarChart,
    CartesianGrid,
    XAxis,
    YAxis,
    Bar,
    Tooltip,
    ResponsiveContainer
} from 'recharts'

const data = [
    { name: 'Schwimen', clicks: 250 },
    { name: 'Klettern', clicks: 130 },
    { name: 'Wassersport', clicks: 100 },
    { name: 'Autofahren', clicks: 800 },
    { name: 'x', clicks: 450 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'Kletteyrn', clicks: 350 },
    { name: 'rUn', clicks: 987 }
]

const BarplotSportsClicks: React.FC = () => {
    return (
        <div id="SportartenClicks" style={{ textAlign: 'center' }}>
            <h2> Aufrufe pro Sporart </h2>
            <div style={{ height: 400, paddingLeft: 50, display: 'flex' }}>
                <ResponsiveContainer width="100%" height="100%">
                    <BarChart
                        margin={{ top: 20, right: 50, left: 0, bottom: 0 }}
                        data={data}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" padding={{ left: 10 }} />
                        <YAxis
                            label={{
                                value: 'Aufrufe',
                                angle: -90,
                                position: 'insideLeft'
                            }}
                            padding={{ bottom: 10 }}
                        />
                        <Tooltip />
                        <Bar dataKey="clicks" fill="#6B9E1F" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </div>
    )
}

export default BarplotSportsClicks
