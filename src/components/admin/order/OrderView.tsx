import React, { useCallback, useEffect, useState } from 'react'
import { makeStyles, Typography } from '@material-ui/core'
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'
import * as ApiResponses from '../../../types/ApiTypes'
import SaveIcon from '@material-ui/icons/Save'
import Fab from '@material-ui/core/Fab'
import { useOrderHook } from './useOrderHook'
import { ConfirmDialog } from '../shared/Dialogs/ConfirmDialog'
import { DraggableListEntry } from './DraggableListEntry'
import { useAppDispatch } from '../../../hooks'
import { updateOrderedPages } from '../../../redux/Slices/orderedPagesSlice'

const useStyles = makeStyles((theme) => ({
    fab: {
        position: 'fixed',
        bottom: theme.spacing(2),
        right: theme.spacing(2)
    }
}))

export const OrderView: React.FC = () => {
    const dispatch = useAppDispatch()
    const classes = useStyles()
    const pages = useOrderHook()
    const [ordered, setOrdered] = useState<ApiResponses.OrderedPages>([])

    useEffect(() => {
        setOrdered(pages)
    }, [pages])

    const moveEntry = useCallback(
        (dragIndex: number, hoverIndex: number) => {
            const dragCard = ordered[dragIndex]
            const newData = [...ordered]
            // move entry from `dragIndex` to hoverIndex in data
            newData.splice(dragIndex, 1)
            newData.splice(hoverIndex, 0, dragCard)

            setOrdered(newData)
        },
        [ordered]
    )

    const saveOrder = useCallback(async () => {
        await fetch('/api/ordered', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(ordered)
        }).then((e) => {
            if (e.ok) {
                dispatch(updateOrderedPages(ordered.map((e) => e.id)))
            }
        })
    }, [ordered])

    const dialogConfirmState = useState(false)

    return (
        <div>
            <Typography variant="h2">
                Reihenfolge der Fragen, Snacks & Bewegungsaufforderungen
            </Typography>
            <div>
                <DndProvider backend={HTML5Backend}>
                    {ordered.map((entry, idx) => (
                        <DraggableListEntry
                            key={entry.id}
                            entry={entry}
                            moveEntry={moveEntry}
                            index={idx}
                        />
                    ))}
                </DndProvider>
            </div>
            <Fab
                color="primary"
                aria-label="save"
                className={classes.fab}
                onClick={() => dialogConfirmState[1](true)}
            >
                <SaveIcon />
            </Fab>
            <ConfirmDialog
                handleCategorie={saveOrder}
                state={dialogConfirmState}
                text={{
                    header: 'Speichern',
                    body: 'Sollen die Änderungen gespeichert werden',
                    confirm: 'Bestätigen',
                    abort: 'Abbrechen'
                }}
            />
        </div>
    )
}
