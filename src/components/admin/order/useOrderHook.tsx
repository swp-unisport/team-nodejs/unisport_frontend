import { useEffect, useState } from 'react'
import { useAppSelector } from '../../../hooks'
import {
    IActivity,
    IQuestion,
    ISnack,
    OrderedPages
} from '../../../types/ApiTypes'

export const useOrderHook = (): OrderedPages => {
    const questionData = useAppSelector(
        (state) => state.questionsData.value
    ) as IQuestion[]
    const snackData = useAppSelector(
        (state) => state.snacksData.value
    ) as ISnack[]
    const activityData = useAppSelector(
        (state) => state.activitiesData.value
    ) as IActivity[]

    const orderedPages = useAppSelector((state) => state.orderedPagesData.value)
    const [orderPages, setOrderPages] = useState<OrderedPages>([])

    useEffect(() => {
        if (orderedPages && questionData && snackData && activityData) {
            let orderArray: OrderedPages = []
            for (let p of orderedPages) {
                let flag = 0
                for (let q of questionData) {
                    if (p === q._id) {
                        orderArray.push({
                            id: q._id,
                            title: q.descriptions['de'],
                            type: 'question'
                        })
                        flag = 1
                        break
                    }
                }
                if (!flag) {
                    for (let s of snackData) {
                        if (p === s._id) {
                            orderArray.push({
                                id: s._id,
                                title: s.title,
                                type: 'snack'
                            })
                            flag = 1
                            break
                        }
                    }
                }
                if (!flag) {
                    for (let a of activityData) {
                        if (p === a._id) {
                            orderArray.push({
                                id: a._id,
                                title: a.title,
                                type: 'activity'
                            })
                            flag = 1
                            break
                        }
                    }
                }
            }
            setOrderPages(orderArray)
        }
    }, [orderedPages, questionData, snackData, activityData])

    console.log(orderPages)
    return orderPages
}
