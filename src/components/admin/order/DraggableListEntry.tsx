import React, { useRef } from 'react'
import { DropTargetMonitor, useDrag, useDrop, XYCoord } from 'react-dnd'
import ImportContactsIcon from '@material-ui/icons/ImportContacts'
import HelpIcon from '@material-ui/icons/Help'
import DirectionsRunIcon from '@material-ui/icons/DirectionsRun'
import { makeStyles, Paper } from '@material-ui/core'
import * as ApiResponses from '../../../types/ApiTypes'

const useStyles = makeStyles((theme) => ({
    entryPaper: {
        padding: '4px 0',
        margin: '2px 2px',
        width: '400px',
        display: 'grid',
        gridTemplateColumns: '30px auto',
        alignItems: 'center'
    },
    icons: {
        justifySelf: 'center'
    }
}))

interface DraggableListEntryProps {
    entry: ApiResponses.OrderedPages[number]
    index: number
    moveEntry: (dragIndex: number, hoverIndex: number) => void
}

interface DragItem {
    index: number
    id: string
    type: string
}

export const DraggableListEntry: React.FC<DraggableListEntryProps> = ({
    entry,
    index,
    moveEntry
}) => {
    const ref = useRef<HTMLDivElement>(null)

    const classes = useStyles()

    const [{ handlerId }, drop] = useDrop({
        accept: 'entry',
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId()
            }
        },
        hover(item: DragItem, monitor: DropTargetMonitor) {
            if (!ref.current) {
                return
            }
            const dragIndex = item.index
            const hoverIndex = index

            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return
            }

            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect()

            // Get vertical middle
            const hoverMiddleY =
                (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

            // Determine mouse position
            const clientOffset = monitor.getClientOffset()

            // Get pixels to the top
            const hoverClientY =
                (clientOffset as XYCoord).y - hoverBoundingRect.top

            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%

            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return
            }

            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return
            }

            // Time to actually perform the action
            moveEntry(dragIndex, hoverIndex)

            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex
        }
    })

    const [{ isDragging }, drag] = useDrag({
        type: 'entry',
        item: () => {
            return { id: entry.id, index }
        },
        collect: (monitor: any) => ({
            isDragging: monitor.isDragging()
        })
    })

    const opacity = isDragging ? 0 : 1
    drag(drop(ref))

    let Icon

    switch (entry.type) {
        case 'snack':
            Icon = ImportContactsIcon
            break
        case 'question':
            Icon = HelpIcon
            break
        case 'activity':
            Icon = DirectionsRunIcon
            break
    }

    return (
        <Paper
            ref={ref}
            data-handler-id={handlerId}
            className={classes.entryPaper}
        >
            <Icon className={classes.icons} />
            <h3>{entry.title}</h3>
        </Paper>
    )
}
