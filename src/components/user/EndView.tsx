import React, { useCallback, useEffect, useState } from 'react'
import {
    CircularProgress,
    Grid,
    ListItem,
    ListItemIcon,
    makeStyles,
    Typography
} from '@material-ui/core'
import Logo from '../utilityComponents/Logo'
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined'
import WizardFooter from './WizardFooter'
import List from '@material-ui/core/List'
import { ListItemProps } from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { useAppSelector } from '../../hooks'
import { IAnswer, ISportResult } from '../../types/ApiTypes'
import axios, { AxiosResponse } from 'axios'
import GreenArea from '../utilityComponents/GreenArea'

const useStyles = makeStyles((theme) => ({
    containerGrid: {
        minHeight: '100vh',
        maxWidth: '600px',
        margin: '0 auto'
    },
    nextBtn: {
        marginRight: '2%'
    },
    prevBtn: {
        marginLeft: '2%'
    },
    whiteText: {
        color: 'white'
    },
    rankSpan: { color: 'white', fontSize: 20, fontWeight: 'bold' }
}))
const EndView = () => {
    const answersData = useAppSelector(
        (state) => state.answersData.value
    ) as IAnswer[]
    const [sports, setSports] = useState<ISportResult[]>([])
    const fetchSports = useCallback(async () => {
        axios
            .post('api/get-matching-sports', { answers: answersData })
            .then((res: AxiosResponse<ISportResult[]>) => {
                //TODO change documentation (201)
                if (res.status == 200) {
                    setTimeout(() => setSports(res.data), 2000)
                } else {
                    console.log('Request failed with HTTP status:', res.status)
                }
            })
            .catch((err) => console.log('error fetching sports list'))
    }, [sports])
    useEffect(() => {
        fetchSports()
    }, [])
    const classes = useStyles()

    function ListItemLink(props: ListItemProps<'a', { button?: true }>) {
        return <ListItem button component="a" {...props} />
    }

    return (
        <Grid
            container
            spacing={3}
            className={classes.containerGrid}
            alignContent="center"
            direction="column"
            alignItems="center"
            justify="center"
        >
            <Logo />
            <GreenArea>
                <HelpOutlineOutlinedIcon
                    color="secondary"
                    opacity="30%"
                    fontSize="large"
                />
                <>
                    {sports.length == 0 && (
                        <Grid
                            container
                            spacing={0}
                            direction="column"
                            alignItems="center"
                            justify="center"
                        >
                            <Typography variant="h2" color="secondary">
                                Die für Sie geeignete Sportarten werden
                                geladen...
                            </Typography>
                            <CircularProgress color="secondary" />
                        </Grid>
                    )}
                </>
                <>
                    {sports.length > 0 && (
                        <div>
                            <Typography
                                color="secondary"
                                align="center"
                                variant="h5"
                            >
                                Aufgrund Ihrer Angaben empfehlen wir Ihnen
                                folgende Sportarten:
                            </Typography>
                            <List
                                component="nav"
                                aria-label="secondary mailbox folders"
                            >
                                {sports.map((sport, index) => {
                                    return (
                                        <ListItemLink
                                            divider
                                            rel="noopener noreferrer"
                                            target="_blank"
                                            href={'//' + sport.url}
                                            key={sport.name}
                                        >
                                            <ListItemIcon>
                                                <span
                                                    className={classes.rankSpan}
                                                >
                                                    {index + 1}
                                                </span>
                                            </ListItemIcon>
                                            <ListItemText
                                                className={classes.whiteText}
                                                primary={sport.name}
                                            />
                                        </ListItemLink>
                                    )
                                })}
                            </List>
                        </div>
                    )}
                </>
            </GreenArea>
            <WizardFooter></WizardFooter>
        </Grid>
    )
}
export default EndView
