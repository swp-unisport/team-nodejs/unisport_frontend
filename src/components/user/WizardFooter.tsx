import React from 'react'
import { useWizard } from 'react-use-wizard'
import NavigateNextOutlinedIcon from '@material-ui/icons/NavigateNextOutlined'
import { NavigateBefore } from '@material-ui/icons'
import { Button, Grid } from '@material-ui/core'

const WizardFooter: React.FC = () => {
    const { previousStep, nextStep, isLastStep } = useWizard()
    return (
        <Grid item xs={12} container justify="space-between">
            <Button
                variant="outlined"
                color="primary"
                onClick={() => previousStep()}
            >
                <NavigateBefore /> Zurück
            </Button>
            {!isLastStep && (
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => nextStep()}
                >
                    <NavigateNextOutlinedIcon /> Weiter
                </Button>
            )}
        </Grid>
    )
}

export default WizardFooter
