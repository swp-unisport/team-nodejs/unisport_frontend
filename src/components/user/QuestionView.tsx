import React, { useState } from 'react'
import { Button, Grid, makeStyles, Slider, Typography } from '@material-ui/core'
import Logo from '../utilityComponents/Logo'
import HelpOutlineOutlinedIcon from '@material-ui/icons/HelpOutlineOutlined'
import NavigateNextOutlinedIcon from '@material-ui/icons/NavigateNextOutlined'
import { useWizard } from 'react-use-wizard'
import { NavigateBefore } from '@material-ui/icons'

import Radio, { RadioProps } from '@material-ui/core/Radio'
import { useAppDispatch, useAppSelector } from '../../hooks'
import { updateAnswers } from '../../redux/Slices/answersSlice'
import { IQuestion } from '../../types/ApiTypes'
import GreenArea from '../utilityComponents/GreenArea'
import AnswerAlert from '../utilityComponents/AnswerAlert'

interface IQuestionProps {
    question: IQuestion
    index: number
}

function valuetext(value: number) {
    return `${value}`
}

const useStyles = makeStyles((theme) => ({
    containerGrid: {
        minHeight: '100vh',
        maxWidth: '600px',
        margin: '0 auto'
    },
    answerContainer: {
        display: 'grid',
        justifyContent: 'space-around',
        gridTemplateColumns: '1fr repeat(4, auto) 1fr',
        alignItems: 'center'
    },
    answerTypography: {
        marginBottom: '25px'
    },
    answerChoices: {},
    nextBtn: {
        marginRight: '2%'
    },
    prevBtn: {
        marginLeft: '2%'
    },
    divider: {
        width: '100%',
        height: '1px',
        border: 'none',
        margin: '1em 0'
    }
}))

const QuestionView = ({ question, index }: IQuestionProps) => {
    const dispatch = useAppDispatch()
    const language = useAppSelector((state) => state.languageData.value)

    const answersData = useAppSelector((state) => state.answersData.value)
    const { handleStep, previousStep, nextStep } = useWizard()
    const classes = useStyles()
    const [selectedOptionValue, setSelectedOptionValue] = React.useState(
        answersData[index].answer == -1
            ? ''
            : 'abcd'.charAt(answersData[index].answer - 1)
    )
    const [isAnswerMissing, setIsAnswerMissing] = useState(false)
    const [sliderValue, setSliderValue] = React.useState(
        answersData[index].weight
    )
    const translateAnswerToNumber = (answer: string): number => {
        const alphabet = ['a', 'b', 'c', 'd']
        return alphabet.indexOf(answer) + 1
    }
    const handleRadioButtonChange = (
        event: React.ChangeEvent<HTMLInputElement>
    ) => {
        setIsAnswerMissing(false)
        setSelectedOptionValue(event.target.value)
    }
    return (
        <Grid
            container
            className={classes.containerGrid}
            alignContent="center"
            direction="column"
            alignItems="center"
            justify="center"
        >
            <AnswerAlert toggle={isAnswerMissing} />
            <Logo />
            <GreenArea>
                <HelpOutlineOutlinedIcon
                    color="secondary"
                    opacity="40%"
                    fontSize="large"
                />
                <Typography color="secondary" align="center" variant="h5">
                    {question.descriptions[language]}
                </Typography>
            </GreenArea>

            <GreenArea>
                <Typography
                    className={classes.answerTypography}
                    id="answer"
                    color="secondary"
                    variant="h6"
                >
                    Answer
                </Typography>
                <div className={classes.answerContainer}>
                    <Typography
                        id="answer"
                        color="secondary"
                        style={{ justifySelf: 'end' }}
                    >
                        Stimme nicht zu
                    </Typography>
                    <Radio
                        checked={selectedOptionValue === 'a'}
                        onChange={handleRadioButtonChange}
                        value="a"
                        name="radio-button-demo"
                        inputProps={{ 'aria-label': 'A' }}
                    />
                    <Radio
                        checked={selectedOptionValue === 'b'}
                        onChange={handleRadioButtonChange}
                        value="b"
                        name="radio-button-demo"
                        inputProps={{ 'aria-label': 'B' }}
                    />
                    <Radio
                        checked={selectedOptionValue === 'c'}
                        onChange={handleRadioButtonChange}
                        value="c"
                        name="radio-button-demo"
                        inputProps={{ 'aria-label': 'C' }}
                    />
                    <Radio
                        checked={selectedOptionValue === 'd'}
                        onChange={handleRadioButtonChange}
                        value="d"
                        name="radio-button-demo"
                        inputProps={{ 'aria-label': 'D' }}
                    />
                    <Typography
                        id="answer"
                        color="secondary"
                        style={{ justifySelf: 'start' }}
                    >
                        Stimme zu
                    </Typography>
                </div>
                <hr className={classes.divider} />
                <Typography
                    className={classes.answerTypography}
                    id="answer"
                    color="secondary"
                    variant="h6"
                >
                    Wie wichtig ist mir diese Antwort
                </Typography>
                <Slider
                    key={`slider-q-${index}-${sliderValue}`}
                    defaultValue={sliderValue}
                    onChangeCommitted={(event, value) =>
                        setSliderValue(parseInt(value.toString()))
                    }
                    aria-labelledby="answer"
                    valueLabelDisplay="on"
                    getAriaValueText={valuetext}
                    step={1}
                    marks
                    color="secondary"
                    min={0}
                    max={3}
                />
            </GreenArea>
            <Grid item xs={12} container justify="space-between">
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => previousStep()}
                >
                    <NavigateBefore /> Zurück
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => {
                        if (selectedOptionValue === '') {
                            setIsAnswerMissing(true)
                            setTimeout(() => setIsAnswerMissing(false), 3000)
                        } else {
                            nextStep().then(() => {
                                dispatch(
                                    updateAnswers({
                                        index: index,
                                        category: question.category,
                                        answer: translateAnswerToNumber(
                                            selectedOptionValue
                                        ),
                                        weight: sliderValue
                                    })
                                )
                            })
                        }
                    }}
                >
                    <NavigateNextOutlinedIcon /> Weiter
                </Button>
            </Grid>
        </Grid>
    )
}
export default QuestionView
