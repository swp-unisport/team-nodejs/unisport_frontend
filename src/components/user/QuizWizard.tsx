import React, { useCallback, useEffect, useState } from 'react'
import axios, { AxiosResponse } from 'axios'
import SnackView from './SnackView'
import QuestionView from './QuestionView'
import ActivityView from './ActivityView'
import { Wizard } from 'react-use-wizard'
import { Page } from '../../types/ApiTypes'
import EndView from './EndView'
import StartView from './StartView'
import { useAppSelector } from '../../hooks'

const QUIZ_ENDPOINT: string = '/api/frontendContainer'
let index = 0
const QuizWizard = () => {
    const [stepComponents, setStepComponents] = useState<JSX.Element[]>([])
    const language = useAppSelector((state) => state.languageData.value)
    const fetchAPI = useCallback(async () => {
        let steps: JSX.Element[] = []
        let response: AxiosResponse<Page[]> = await axios.get(QUIZ_ENDPOINT)

        //TODO change
        response.data.forEach((element: Page) => {
            switch (element.type) {
                case 'snack':
                    steps.push(
                        <SnackView
                            key={element.id}
                            text={element.descriptions[language]}
                            background={element.imageUrl}
                        />
                    )
                    break
                case 'question':
                    steps.push(
                        <QuestionView
                            index={index}
                            key={element.id}
                            question={element}
                        />
                    )
                    index++
                    break
                case 'activity':
                    steps.push(
                        <ActivityView
                            key={element.id}
                            text={element.descriptions[language]}
                            background={element.imageUrl}
                        />
                    )
                    break
                default:
                    steps.push(<div key="err_000">Error Step</div>)
            }
        })

        //add first page and last page
        steps.unshift(<StartView key="start_v1" />)
        //TODO make it trackable
        steps.push(<EndView key="end_v1" />)
        setStepComponents(steps)
    }, [])
    useEffect(() => {
        fetchAPI()
    }, [])

    return <Wizard>{stepComponents}</Wizard>
}
export default QuizWizard
