import React from 'react'
import { Button, Grid, makeStyles, Typography } from '@material-ui/core'
import { DirectionsRun } from '@material-ui/icons'
import Logo from '../utilityComponents/Logo'
import GreenArea from '../utilityComponents/GreenArea'
import { useWizard } from 'react-use-wizard'
import { useAppDispatch, useAppSelector } from '../../hooks'
import { updateLanguage } from '../../redux/Slices/languageSlice'
import { Language } from '../../types/ApiTypes'

const useStyles = makeStyles((theme) => ({
    containerGrid: {
        minHeight: '100vh'
    },
    paper: {
        padding: '20px',
        textAlign: 'center',
        backgroundColor: '#8bc34a'
    },
    margin: {
        margin: theme.spacing(1)
    },
    center: {
        display: 'flex',
        justifyContent: 'center',
        marginBottom: '20px'
    }
}))
const StartView = () => {
    const { handleStep, previousStep, nextStep } = useWizard()

    const language = useAppSelector((state) => state.languageData.value)
    const dispatch = useAppDispatch()

    const changeLanguage = (lang: Language) => {
        dispatch(updateLanguage(lang))
    }

    const classes = useStyles()
    return (
        <Grid
            container
            alignContent="center"
            direction="column"
            alignItems="center"
            justify="center"
            className={classes.containerGrid}
        >
            <Logo />
            <Grid item xs={12} md={10}>
                <GreenArea style={{ marginBottom: '20px' }}>
                    <Typography color="secondary" align="center" variant="h5">
                        Dies ist ein Assistent zur Auswahl der für dich
                        passenden Sportart.
                    </Typography>
                </GreenArea>
                <div className={classes.center}>
                    <Button
                        size="small"
                        className={classes.margin}
                        variant={
                            language == Language.GERMAN
                                ? 'contained'
                                : undefined
                        }
                        onClick={() => changeLanguage(Language.GERMAN)}
                    >
                        GER
                    </Button>
                    <Button
                        size="small"
                        className={classes.margin}
                        variant={
                            language == Language.ENGLISH
                                ? 'contained'
                                : undefined
                        }
                        onClick={() => changeLanguage(Language.ENGLISH)}
                    >
                        ENG
                    </Button>
                    <Button
                        size="small"
                        className={classes.margin}
                        variant={
                            language == Language.FRENCH
                                ? 'contained'
                                : undefined
                        }
                        onClick={() => changeLanguage(Language.FRENCH)}
                    >
                        FRA
                    </Button>
                </div>
            </Grid>
            <Grid item xs={4} md={4}>
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => nextStep()}
                >
                    <DirectionsRun /> Starten
                </Button>
            </Grid>
        </Grid>
    )
}
export default StartView
