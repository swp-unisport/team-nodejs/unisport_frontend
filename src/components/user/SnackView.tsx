import React from 'react'
import defaultBackground from '../../assets/snack_green.png'
import {
    Button,
    Card,
    CardActionArea,
    CardActions,
    CardMedia,
    Grid,
    makeStyles,
    Typography
} from '@material-ui/core'
import NavigateNextOutlinedIcon from '@material-ui/icons/NavigateNextOutlined'
import { NavigateBefore } from '@material-ui/icons'
import { useWizard } from 'react-use-wizard'
import WizardFooter from './WizardFooter'

interface ISnackProps {
    background?: string
    text: string
}

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: '600px',
        margin: '0 auto',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    card: {
        width: '100%',
        marginBottom: '30px',
        position: 'relative'
    },
    backgroundImg: {
        opacity: 0.7,
        width: '100%'
    },
    snackText: {
        opacity: 0.9,
        position: 'absolute',
        top: '40%',
        fontSize: 'large',
        color: 'white',
        width: '100%'
    },
    boldQuestion: {
        marginTop: '10px',
        marginBottom: '10px',
        fontWeight: 'bold'
    },
    nextBtn: {
        marginRight: '2%'
    },
    prevBtn: {
        marginLeft: '2%'
    }
}))
const SnackView = ({ text, background = defaultBackground }: ISnackProps) => {
    const classes = useStyles()
    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.backgroundImg}
                    title="SnackView background"
                    component="img"
                    image={background}
                />
                <Typography
                    align="center"
                    className={classes.snackText}
                    variant="h6"
                    color="textSecondary"
                    component="p"
                >
                    {text}
                </Typography>
                <CardActions>
                    <Typography
                        className={classes.boldQuestion}
                        variant="h5"
                        color="primary"
                    >
                        Wusstest du nicht ?
                    </Typography>
                </CardActions>
            </Card>
            <WizardFooter></WizardFooter>
        </div>
    )
}
export default SnackView
