import React, { useEffect, useState } from 'react'
import defaultBackground from '../../assets/snack_green.png'
import {
    Button,
    Card,
    CardActions,
    CardMedia,
    LinearProgress,
    makeStyles,
    Typography
} from '@material-ui/core'
import NavigateNextOutlinedIcon from '@material-ui/icons/NavigateNextOutlined'
import NavigateBeforeOutlinedIcon from '@material-ui/icons/NavigateBeforeOutlined'
import AccessTimeIcon from '@material-ui/icons/AccessTime'
import { useWizard } from 'react-use-wizard'

interface IActivityProps {
    background?: string
    text: string
    duration?: number
}

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: '600px',
        margin: '0 auto',
        minHeight: '100vh',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center'
    },
    card: {
        width: '100%',
        marginBottom: '30px',
        position: 'relative'
    },
    navButtons: {
        display: 'grid',
        gridTemplateRows: '1fr 1fr',
        gridTemplateColumns: '1fr 1fr',
        gap: '10px',
        width: '250px',
        margin: '0 auto'
    },
    backgroundImg: {
        width: '100%'
    },
    boldQuestion: {
        marginTop: '10px',
        marginBottom: '10px',
        fontWeight: 'bold'
    },
    remainingSeconds: {
        textAlign: 'right'
    }
}))

interface ActivityState {
    progressPercentage: number
    remainingSeconds?: number
    startTime?: number
}

const ActivityView = ({
    text,
    background = defaultBackground,
    duration = 15
}: IActivityProps) => {
    const classes = useStyles()
    const { nextStep, previousStep } = useWizard()

    const [state, setState] = useState<ActivityState>({
        progressPercentage: 0,
        remainingSeconds: undefined,
        startTime: undefined
    })

    useEffect(() => {
        if (!state.startTime) {
            console.warn('updateTimer called but startTime is undefined')
            return
        }
        if (
            state.remainingSeconds !== undefined &&
            state.remainingSeconds <= 0
        ) {
            nextStep()
            return
        }
        setTimeout(() => {
            if (!state.startTime) {
                console.warn('updateTimer called but startTime is undefined')
                return
            }
            const elapsedTime = (Date.now() - state.startTime) / 1000
            const remainingSeconds = Math.ceil(duration - elapsedTime)
            const percentage = elapsedTime / duration
            setState({
                progressPercentage: percentage * 100,
                remainingSeconds,
                startTime: state.startTime
            })
        }, 100)
    }, [state])

    const startTimer = () => {
        console.log('startTimer')

        setState({
            progressPercentage: 0,
            remainingSeconds: duration,
            startTime: Date.now()
        })
    }

    return (
        <div className={classes.root}>
            <Card className={classes.card}>
                <CardMedia
                    className={classes.backgroundImg}
                    title="SnackView background"
                    component="img"
                    image={background}
                />
                <CardActions>
                    <Typography
                        className={classes.boldQuestion}
                        variant="h5"
                        color="primary"
                    >
                        {text}
                    </Typography>
                </CardActions>
            </Card>
            <div className={classes.navButtons}>
                <Button
                    startIcon={<AccessTimeIcon />}
                    variant="outlined"
                    onClick={() => startTimer()}
                    style={{ gridColumn: '1 / span 2' }}
                >
                    Start
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => previousStep()}
                >
                    <NavigateBeforeOutlinedIcon /> Zurück
                </Button>
                <Button
                    variant="outlined"
                    color="primary"
                    onClick={() => nextStep()}
                >
                    <NavigateNextOutlinedIcon /> Skip
                </Button>
            </div>
            <p className={classes.remainingSeconds}>
                {state.startTime ? `${state.remainingSeconds}s` : ' '}
            </p>
            <LinearProgress
                variant="determinate"
                value={state.progressPercentage}
            />
        </div>
    )
}
export default ActivityView
