import { red } from '@material-ui/core/colors'
import { createMuiTheme, Theme } from '@material-ui/core/styles'

//TODO: maybe work on this custom theme instead of creating the theme in App.tsx
const mainTheme: Theme = createMuiTheme({
    palette: {
        primary: {
            main: '#6B9E1E'
        },
        secondary: {
            //TODO: TBD
            main: '#ffffff'
        },
        error: {
            main: red.A400
        },
        background: {
            default: '#ffffff'
        }
    },
    typography: {
        h2: {
            fontSize: '1.5em'
        }
    }
})

export default mainTheme
