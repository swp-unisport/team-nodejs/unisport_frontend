import { CssBaseline, useMediaQuery } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/styles'
import React, { Suspense, useEffect } from 'react'
import { Switch, Route } from 'react-router-dom'
import QuizWizard from './components/user/QuizWizard'
import mainTheme from './constants/themes'
import { Provider } from 'react-redux'
import { store } from './redux/store'

const AdminComponent = React.lazy(
    () => import('./components/admin/adminHome/AdminHome')
)

function App() {
    // use browser history by default, can be overwritten for testing
    // code from https://material-ui.com/customization/palette/
    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: light)')

    //TODO this change have to be investigated further, taking the default dark/light modes cause ugly visualization
    // in User side
    const theme = React.useMemo(() => {
        mainTheme.palette.type = prefersDarkMode ? 'dark' : 'light'
        return mainTheme
    }, [prefersDarkMode])

    useEffect(() => {
        document.body.style.backgroundColor = theme.palette.background.default
    }, [theme])

    return (
        <Provider store={store}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <Switch>
                    <Route path="/admin">
                        <Suspense
                            fallback={<div>Loading AdminComponent...</div>}
                        >
                            <AdminComponent />
                        </Suspense>
                    </Route>
                    <Route exact path="/">
                        <QuizWizard />
                    </Route>
                </Switch>
            </ThemeProvider>
        </Provider>
    )
}

export default App
